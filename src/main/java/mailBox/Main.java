package mailBox;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.mailsModels.CondoMailBox;
import mailBox.models.mailsModels.CondoMailBoxList;
import mailBox.models.mailsModels.Mail;
import mailBox.models.roomModels.CondoRoom;
import mailBox.models.roomModels.CondoRoomList;
import mailBox.services.mailServices.MailDataSource;
import mailBox.services.mailServices.MailFileDataSource;
import mailBox.services.roomServices.CondoRoomDataSource;
import mailBox.services.roomServices.CondoRoomFileDataSource;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/home.fxml"));
        primaryStage.setTitle("6210451055");
        primaryStage.setScene(new Scene(root, 1000, 800));
        primaryStage.getScene()
                .getStylesheets()
                .add("org/kordamp/bootstrapfx/bootstrapfx.css");

        primaryStage.show();
    }

    public static void main(String[] args) throws IOException {
        launch(args); }
}
