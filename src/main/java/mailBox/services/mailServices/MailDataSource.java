package mailBox.services.mailServices;

import mailBox.models.mailsModels.CondoMailBox;
import mailBox.models.mailsModels.CondoMailBoxList;

import java.io.IOException;

public interface MailDataSource {
//    CondoMailBoxList getMailBoxData() throws IOException;

    void clearFile();

    CondoMailBoxList getMailBoxData(CondoMailBoxList condoMailBoxList) throws IOException;

//    void setMailBoxData(CondoMailBoxList condoMailBoxList);//ERROR method java.io.StreamCorruptedException: invalid stream header: 7371007E hotfix by add obj by array obg by obg

    void addMailBoxData(CondoMailBox condoMailBox);
}
