package mailBox.services.mailServices;

import mailBox.models.mailsModels.CondoMailBox;
import mailBox.models.mailsModels.CondoMailBoxList;
import mailBox.models.roomModels.CondoRoom;

import java.io.*;

public class MailFileDataSource implements MailDataSource{
    private String fileDirectoryName;
    private String fileName;
    private CondoMailBoxList condoMailBoxList;

    public MailFileDataSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData(CondoMailBoxList condoMailBoxList) throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);

        File fileLine = new File(filePath);
        FileReader fileReader = new FileReader(fileLine);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        if ((line = reader.readLine()) != null){
            try {
                FileInputStream inputStream = new FileInputStream(file);
                ObjectInputStream os = new ObjectInputStream(inputStream);
                boolean isReading = true;
                try {
                    while (isReading) {
                        CondoMailBox condoMailBox = (CondoMailBox) os.readObject();
                        condoMailBoxList.add(condoMailBox);
                        os = new ObjectInputStream(inputStream);
                    }
                    os.close();
                } catch (EOFException e) {
                    isReading = false;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void clearFile(){
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            writer.write("");
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write "+filePath);
        }
    }

    @Override
    public CondoMailBoxList getMailBoxData(CondoMailBoxList condoMailBoxList) throws IOException {
        condoMailBoxList = new CondoMailBoxList();
        readData(condoMailBoxList);
        return condoMailBoxList;
    }

    @Override
    public void addMailBoxData(CondoMailBox condoMailBox){
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file,true);
            ObjectOutputStream os = new ObjectOutputStream(fileOutputStream);

            os.writeObject(condoMailBox);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
                e.printStackTrace();
        }
    }
}
