package mailBox.services.roomServices;

import mailBox.models.roomModels.CondoRoom;
import mailBox.models.roomModels.CondoRoomList;

import java.io.IOException;

public interface CondoRoomDataSource {
    CondoRoomList getCondoRoomData();

    void addCondoRoomData(CondoRoom condoRoom);

    void setCondoRoomData(CondoRoomList condoRoomData);

    void addMemberToRoom(CondoRoomList condoRoomList,CondoRoom condoRoom);
}
