package mailBox.services.roomServices;

import mailBox.models.roomModels.CondoRoom;
import mailBox.models.roomModels.CondoRoomList;
import mailBox.models.roomModels.MediumCondoRoom;
import mailBox.models.roomModels.SmallCondoRoom;

import java.io.*;
import java.util.ArrayList;

public class CondoRoomFileDataSource implements CondoRoomDataSource {
    private String fileDirectoryName;
    private String fileName;
    private CondoRoomList condoRooms;

    public CondoRoomFileDataSource(String fileDirectoryName, String fileName) throws FileNotFoundException {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine())!=null) {
            String[] data = line.split(",");
            CondoRoom room;
            if (data[0].trim().equalsIgnoreCase("Medium Room-2 Beds")){
                room = new MediumCondoRoom(data[2].trim(), data[3].trim().substring(1),data[4].trim().substring(1),data[5].trim(),data[1].trim());
            }else{
                room = new SmallCondoRoom(data[2].trim(), data[3].trim().substring(1),data[4].trim().substring(1),data[5].trim(),data[1].trim());
            }
            condoRooms.add(room);
        }
        reader.close();
    }

    @Override
    public CondoRoomList getCondoRoomData() {
        try {
            condoRooms = new CondoRoomList();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName+" not found");
        } catch (IOException e) {
            System.err.println("IOException from reading "+ this.fileName);
        }
        return condoRooms;
    }

    @Override
    public void addCondoRoomData(CondoRoom condoRoom){
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file,true);
            BufferedWriter writer = new BufferedWriter(fileWriter);
//            String line = condoRoom.getOwner() + ","
//                    + condoRoom.getBuilding() + ","
//                    + "F"+condoRoom.getFloor() + ","
//                    + "R"+condoRoom.getRoom() +","
//                    + condoRoom.getRoomType() + ","
//                    + condoRoom.getMember();
            writer.append(condoRoom.toCSV());
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            System.err.println("Cannot write "+filePath);
        }
    }

    @Override
    public void setCondoRoomData(CondoRoomList condoRoomList) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (CondoRoom condoRoom: condoRoomList.toList()){
//                String line = condoRoom.getOwner() + ","
//                        + condoRoom.getBuilding() + ","
//                        +"F"+ condoRoom.getFloor() + ","
//                        +"R"+ condoRoom.getRoom() +","
//                        + condoRoom.getRoomType() + ","
//                        + condoRoom.getMember();
                writer.append(condoRoom.toCSV());
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write "+filePath);
        }
    }

    @Override
    public void addMemberToRoom(CondoRoomList condoRoomList, CondoRoom condoRoom) {
        for (CondoRoom s : condoRoomList.toList()){
            if (s.getRoomID().equals(condoRoom.getRoomID())){
                condoRoomList.remove(s);
                break;
            }
        }
        setCondoRoomData(condoRoomList);
    }
}
