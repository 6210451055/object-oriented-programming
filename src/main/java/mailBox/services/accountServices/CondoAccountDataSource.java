package mailBox.services.accountServices;

import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;

import java.io.IOException;

public interface CondoAccountDataSource {
    CondoAccountList getCondoAccountsData();
    void setCondoAccountsData(CondoAccountList condoAccountsData);
    void addCondoAccountData(CondoAccount condoAccount);
}
