package mailBox.services.accountServices;

import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AccountFileDataSource implements CondoAccountDataSource {

    private String fileDirectoryName;
    private String fileName;
    private CondoAccountList condoAccounts;

    public AccountFileDataSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine())!=null) {
            String[] data = line.split(",");
            if (fileName.equals("CondoAccount.csv")){
                CondoAccount account = new CondoAccount(data[0].trim(),data[1].trim(),data[2].trim(),data[3].trim(),data[4].trim(),data[5].trim());
                condoAccounts.add(account);
            }
        }
        reader.close();
    }

    @Override
    public CondoAccountList getCondoAccountsData() {
        try {
            condoAccounts = new CondoAccountList();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName+" not found");
        } catch (IOException e) {
            System.err.println("IOException from reading "+ this.fileName);
        }
        return  condoAccounts;
    }

    @Override
    public void setCondoAccountsData(CondoAccountList condoAccounts) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            if (fileName.equals("CondoAccount.csv")){
                for (CondoAccount condoAccount: condoAccounts.toList()){
                    String line = condoAccount.getAccountLevel() + ","
                            + condoAccount.getName() + ","
                            + condoAccount.getId() + ","
                            + condoAccount.getPassword() + ","
                            + condoAccount.getRecentLogin() + ","
                            + condoAccount.getAccountProfilePicture();
                    writer.append(line);
                    writer.newLine();
                }
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write "+filePath);
        }
    }

    @Override
    public void addCondoAccountData(CondoAccount condoAccount){
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy / HH:mm:ss");
        String formattedDate = myDateObj.format(myFormatObj);

        try {
            fileWriter = new FileWriter(file,true);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            if (fileName.equals("CondoAccount.csv")){
                String line = condoAccount.getAccountLevel() + ","
                        + condoAccount.getName() + ","
                        + condoAccount.getId() + ","
                        + condoAccount.getPassword()+ ","
                        + condoAccount.getRecentLogin()+ ","
                        + condoAccount.getAccountProfilePicture();
                writer.append(line);
                writer.newLine();
            }
            writer.flush();
        } catch (IOException e) {
            System.err.println("Cannot write "+filePath);
        }
    }

}
