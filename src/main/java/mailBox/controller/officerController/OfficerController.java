package mailBox.controller.officerController;

import mailBox.controller.accountManagementController.ChangePasswordController;
import mailBox.controller.accountManagementController.ResidentRegisterController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.services.accountServices.CondoAccountDataSource;

import java.io.IOException;


public class OfficerController {
    private CurrentUser currentUser;
    private CondoAccount loginAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountDataSource accountLoginDataSource;
    private CondoAccountList accounts;

    @FXML Button registerBtn;
    @FXML Button addResidentBtn;
    @FXML Button showCondoRoomBtn;
    @FXML Button changePasswordBtn;
    @FXML Button mailBoxBtn;
    @FXML Label currentAccLabel;

    @FXML public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccLabel.setText("OFFICER : "+currentUser.getCurrentAccount().getName());
            }
        });
    }
    @FXML public void handleShowCondoRoomBtnOnAction(ActionEvent event)throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/condoRoomList.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        CondoRoomListController condoRoomListController = loader.getController();
        condoRoomListController.setCurrentUser(currentUser);

        stage.show();
    }
    @FXML public void handleRegisterBtnOnAction(ActionEvent event)throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/residentRegister.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        ResidentRegisterController residentRegisterController = loader.getController();
        residentRegisterController.setCurrentUser(currentUser);

        stage.show();
    }

    @FXML public void handleAddResidentBtnOnAction(ActionEvent event)throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/addResidentToRoom.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        AddResidentToRoomController addResidentToRoomController = loader.getController();
        addResidentToRoomController.setCurrentUser(currentUser);

        stage.show();
    }

    @FXML public void handleChangePasswordBtnOnAction (ActionEvent event) throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/changePassword.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        ChangePasswordController changePasswordController = loader.getController();
        changePasswordController.setCurrentUser(currentUser);

        stage.show();
    }

    @FXML public void handleMailBoxBtnOnAction (ActionEvent event) throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/officerMailBox.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        OfficerMailBoxListController officerMailBoxListController = loader.getController();
        officerMailBoxListController.setCurrentUser(currentUser);

        stage.show();
    }

    @FXML public void handleLogoutBtnOnAction (ActionEvent event) throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        stage.show();
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}
