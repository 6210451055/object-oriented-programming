package mailBox.controller.officerController;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;

import java.io.IOException;

public class CreateNewMailController {
    private CurrentUser currentUser;
    private CondoAccount condoAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountList accounts;

    @FXML Button mailBtn;
    @FXML Button documentBtn;
    @FXML Button parcelBtn;
    @FXML Button backBtn;
    @FXML Label currentAccLabel;

    @FXML public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccLabel.setText("OFFICER : "+currentUser.getCurrentAccount().getName());
            }
        });
        accountDataSource = new AccountFileDataSource("data", "CondoAccount.csv");
        accounts = accountDataSource.getCondoAccountsData();
    }


    @FXML public void handleMailBtnOnAction(ActionEvent event)throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/mailCreate.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        MailCreateController mailCreateController = loader.getController();
        mailCreateController.setCurrentUser(currentUser);

        stage.show();
    }
    @FXML public void handleDocumentBtnOnAction(ActionEvent event)throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/mailDocumentCreate.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        MailDocumentCreateController mailDocumentCreateController = loader.getController();
        mailDocumentCreateController.setCurrentUser(currentUser);

        stage.show();
    }
    @FXML public void handleParcelBtnOnAction(ActionEvent event)throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/mailParcelCreate.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        MailParcelCreateController mailParcelCreateController = loader.getController();
        mailParcelCreateController.setCurrentUser(currentUser);

        stage.show();
    }

    @FXML public void handleBackBtnOnAction(ActionEvent event)throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/officerMailBox.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        OfficerMailBoxListController officerMailBoxListController = loader.getController();
        officerMailBoxListController.setCurrentUser(currentUser);

        stage.show();
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}
