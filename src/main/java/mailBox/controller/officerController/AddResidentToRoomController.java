package mailBox.controller.officerController;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mailBox.controller.residentController.ResidentController;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.models.roomModels.CondoRoom;
import mailBox.models.roomModels.CondoRoomList;
import mailBox.models.roomModels.MediumCondoRoom;
import mailBox.models.roomModels.SmallCondoRoom;
import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;
import mailBox.services.roomServices.CondoRoomDataSource;
import mailBox.services.roomServices.CondoRoomFileDataSource;

import java.io.FileNotFoundException;
import java.io.IOException;

public class AddResidentToRoomController {
    private CurrentUser currentUser;
    private CondoAccount condoAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountList accounts;
    private CondoRoom condoRoom;
    private CondoRoomDataSource condoRoomDataSource;
    private CondoRoomList rooms;

    @FXML private Button backBtn;
    @FXML private Button confirmBtn;
    @FXML private TextField nameTextField;
    @FXML private TextField lastNameTextField;
    @FXML private ChoiceBox<String> buildingChoiceBox;
    @FXML private ChoiceBox<String> floorChoiceBox;
    @FXML private ChoiceBox<String> roomChoiceBox;

    @FXML public void initialize() throws FileNotFoundException {
        accountDataSource = new AccountFileDataSource("data", "CondoAccount.csv");
        condoRoomDataSource = new CondoRoomFileDataSource("data","CondoRoom.csv");
        accounts = accountDataSource.getCondoAccountsData();
        buildingChoiceBox.getItems().addAll("A","B");
        buildingChoiceBox.setValue("");
        floorChoiceBox.getItems().addAll("01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20");
        floorChoiceBox.setValue("");
        roomChoiceBox.getItems().addAll("Room 01 : 1 bed","Room 02 : 1 bed","Room 03 : 2 beds","Room 04 : 1 bed","Room 05 : 1 bed","Room 06 : 2 beds","Room 07 : 4 beds","Room 08 : 1 bed","Room 09 : 2 beds","Room 10 : 1 bed");
        roomChoiceBox.setValue("");
    }

    @FXML public void handleConfirmBtnOnAction (ActionEvent event) throws IOException {
        if ((nameTextField.getText().matches("[a-zA-Z]+$")) && lastNameTextField.getText().matches("[a-zA-Z]+$")){

            String name = nameTextField.getText().substring(0, 1).toUpperCase() + nameTextField.getText().substring(1) + " " + lastNameTextField.getText().substring(0, 1).toUpperCase()+lastNameTextField.getText().substring(1);
            if (accounts.isCorrectName(name)){
                if (buildingChoiceBox.getValue().matches("[A-B]") && floorChoiceBox.getValue().matches("[0-9]+$") && roomChoiceBox.getValue().substring(5,7).matches("[0-9a-zA-Z]+$")) {
//                    CondoRoom condoRoom = new CondoRoom(name,buildingChoiceBox.getValue(),floorChoiceBox.getValue(),roomChoiceBox.getValue().substring(5,7),"","1");
                    int room = Integer.parseInt(roomChoiceBox.getValue().substring(5,7));
                    CondoRoom condoRoom;
                    if (room%3 == 0){
                        condoRoom = new MediumCondoRoom(buildingChoiceBox.getValue(),floorChoiceBox.getValue(),roomChoiceBox.getValue().substring(5,7),"1",name);
                    }else {
                        condoRoom = new SmallCondoRoom(buildingChoiceBox.getValue(),floorChoiceBox.getValue(),roomChoiceBox.getValue().substring(5,7),"1",name);
                    }
                    CondoRoomList condoRoomList= condoRoomDataSource.getCondoRoomData();
                    if (!(condoRoomList.isInhabitedRoom(condoRoom))) {
                        condoRoom.setRoomMemberStatus();
                        condoRoomDataSource.addCondoRoomData(condoRoom);
                        Platform.runLater(()->{
                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.initStyle(StageStyle.UTILITY);
                            alert.setTitle("Register");
                            alert.setHeaderText(null);
                            alert.setContentText("Register success!");
                            alert.showAndWait();
                            nameTextField.clear();
                            lastNameTextField.clear();
                        });
                    } else if ((condoRoomList.isInhabitedRoom(condoRoom))) {
                        rooms = condoRoomDataSource.getCondoRoomData();
                        CondoRoomList condoRoomList1 = condoRoomDataSource.getCondoRoomData();
                        condoRoom = condoRoomList1.getExpectRoom(condoRoom);
                        if (condoRoom.addMember(name)) {
                            condoRoom.setRoomMemberStatus();
                            condoRoomDataSource.addMemberToRoom(rooms,condoRoom);
                            condoRoomDataSource.addCondoRoomData(condoRoom);
                            Platform.runLater(()->{
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.initStyle(StageStyle.UTILITY);
                                alert.setTitle("Register");
                                alert.setHeaderText(null);
                                alert.setContentText("Register success!");
                                alert.showAndWait();
                                nameTextField.clear();
                                lastNameTextField.clear();
                            });
                        } else {
                            Platform.runLater(() -> {
                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                alert.initStyle(StageStyle.UTILITY);
                                alert.setTitle("Register failed");
                                alert.setHeaderText(null);
                                alert.setContentText("Register failed! .  This room is full.");
                                alert.showAndWait();
                            });
                        }
                    }
                } else {
                    Platform.runLater(()->{
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.initStyle(StageStyle.UTILITY);
                        alert.setTitle("Register failed");
                        alert.setHeaderText(null);
                        alert.setContentText("Please ensure every room choice box is chooses correctly.");
                        alert.showAndWait();
                    });
                }
            }else {
                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initStyle(StageStyle.UTILITY);
                    alert.setTitle("Register failed");
                    alert.setHeaderText(null);
                    alert.setContentText("Register failed! . This person doesn't have account yet, Please register first");
                    alert.showAndWait();
                });
            }
        } else {
            Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Register failed");
                alert.setHeaderText(null);
                alert.setContentText("Register failed! . This name contain characters that aren't allowed please ensure every field is filled correctly.");
                alert.showAndWait();
            });
        }
    }

    @FXML public void handleBackBtnOnAction (ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/officer.fxml"));
        stage.setScene(new Scene(loader.load(), 1000, 800));

        OfficerController officerController = loader.getController();
        officerController.setCurrentUser(currentUser);

        stage.show();
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}
