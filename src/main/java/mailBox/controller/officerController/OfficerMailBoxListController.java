package mailBox.controller.officerController;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.StageStyle;
import mailBox.controller.officerController.OfficerController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mailBox.controller.officerController.CreateNewMailController;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.models.mailsModels.CondoMailBox;
import mailBox.models.mailsModels.CondoMailBoxList;
import mailBox.models.mailsModels.Mail;
import mailBox.models.roomModels.CondoRoom;
import mailBox.models.roomModels.CondoRoomList;
import mailBox.models.roomModels.SmallCondoRoom;
import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;
import mailBox.services.mailServices.MailDataSource;
import mailBox.services.mailServices.MailFileDataSource;
import mailBox.services.roomServices.CondoRoomDataSource;
import mailBox.services.roomServices.CondoRoomFileDataSource;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class OfficerMailBoxListController {
    private CurrentUser currentUser;
    private CondoAccount condoAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountDataSource accountLoginDataSource;
    private CondoAccountList accounts;
    private CondoRoom condoRoom;
    private CondoRoomDataSource condoRoomDataSource;
    private CondoRoomList condoRoomList;
    private Mail mail;
    private CondoMailBox condoMailBox;
    private CondoMailBox selectedMailBox;
    private CondoMailBoxList condoMailBoxList;
    private MailDataSource mailDataSource;
    private ObservableList<CondoMailBox> condoMailBoxObservableList;

    @FXML private Button createNewMailBtn;
    @FXML private Button backBtn;
    @FXML private Button residentReceivedBtn;
    @FXML private Button seeBtn;
    @FXML private Button searchByRoomBtn;
    @FXML private TextField receiverNameTextField;
    @FXML private TextField receiverLastNameTextField;
    @FXML private ChoiceBox<String> buildingChoiceBox;
    @FXML private ChoiceBox<String> floorChoiceBox;
    @FXML private ChoiceBox<String> roomChoiceBox;
    @FXML private ImageView mailImage;
    @FXML private TableView<CondoMailBox> mailBoxTableView;


    @FXML public void initialize() throws IOException {
        accountDataSource = new AccountFileDataSource("data", "CondoAccount.csv");
        condoRoomDataSource = new CondoRoomFileDataSource("data","CondoRoom.csv");
        mailDataSource = new MailFileDataSource("data","MailBox.csv");
        buildingChoiceBox.getItems().addAll("A","B");
        buildingChoiceBox.setValue("");
        floorChoiceBox.getItems().addAll("01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20");
        floorChoiceBox.setValue("");
        roomChoiceBox.getItems().addAll("Room 01 : 1 bed","Room 02 : 1 bed","Room 03 : 2 beds","Room 04 : 1 bed","Room 05 : 1 bed","Room 06 : 2 beds","Room 07 : 4 beds","Room 08 : 1 bed","Room 09 : 2 beds","Room 10 : 1 bed");
        roomChoiceBox.setValue("");
        mailImage.setImage(new Image("/images/icon/KuIcon.jpg"));
        residentReceivedBtn.setDisable(true);

        showMailBoxData("standard");

        mailBoxTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                showSelectedMail(newValue);
                selectedMailBox = newValue;
            }
        });
    }


    @FXML public void handleCreateNewMailBtnOnAction(ActionEvent event)throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/createNewMail.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        CreateNewMailController createNewMailController = loader.getController();
        createNewMailController.setCurrentUser(currentUser);

        stage.show();
    }

    @FXML public void handleSearchByRoomBtnBtnOnAction(ActionEvent event)throws IOException {
        if (buildingChoiceBox.getValue().matches("[A-B]") && floorChoiceBox.getValue().matches("[0-9]+$") && roomChoiceBox.getValue().substring(5,7).matches("[0-9a-zA-Z]+$")){
            showMailBoxData("expect roomID");
        }else {
            Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Warning!");
                alert.setHeaderText(null);
                alert.setContentText("Please select your roomID first! .");
                alert.showAndWait();
            });
        }
    }

    @FXML public void handleSeeBtnOnAction(ActionEvent event)throws IOException {
        if (seeBtn.getText().equalsIgnoreCase("See All Mail")){
            seeBtn.setText("See Only Received Mail");
            showMailBoxData("see all mail");
        }else{
            seeBtn.setText("See All Mail");
            showMailBoxData("see only received mail");
        }
    }

    @FXML public void handleResidentReceivedBtnOnAction(ActionEvent event)throws IOException {
        condoMailBoxList = new CondoMailBoxList();
        condoMailBoxList = mailDataSource.getMailBoxData(condoMailBoxList);
        condoRoom = null;
        condoRoom = selectedMailBox.getMail().getReceiver();
        String name = receiverNameTextField.getText()+" "+receiverLastNameTextField.getText();

        if (condoRoom.isCorrectOwnerName(name,condoRoom)){
            selectedMailBox.setReceiveStatus("Received");
            selectedMailBox.setResidentReceivingMail(name);
            selectedMailBox.setReceiveMailFromOfficer(currentUser.getCurrentAccount().getName());
            selectedMailBox.setDateTimeResidentReceived(getDateTime());
            for (CondoMailBox s: condoMailBoxList.toList()){
                if (s.getMail().getUniqueField().equals(selectedMailBox.getMail().getUniqueField()) && s.getReceiveStatus().equalsIgnoreCase("Unreceived")){
                    condoMailBoxList.remove(s);
                    condoMailBoxList.add(selectedMailBox);
                    break;
                }
            }
            mailDataSource.clearFile();
            for (CondoMailBox mailBox: condoMailBoxList.toList()){
                mailDataSource.addMailBoxData(mailBox);
            }
            mailBoxTableView.refresh();
            Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Mail Receive Successful!");
                alert.setHeaderText(null);
                alert.setContentText("Mail has been received.");
                alert.showAndWait();
            });
        }else {
            Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Mail Receive Failed!");
                alert.setHeaderText(null);
                alert.setContentText("The receiver name does not match to the receiver mail");
                alert.showAndWait();
            });
        }
    }

    @FXML public void handleBackBtnOnAction(ActionEvent event)throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/officer.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        OfficerController officerController = loader.getController();
        officerController.setCurrentUser(currentUser);

        stage.show();
    }

    private void showMailBoxData(String showStatus) throws IOException {
        condoMailBoxList = new CondoMailBoxList();
        condoMailBoxObservableList = FXCollections.observableArrayList(condoMailBoxList.toList());

        TableColumn<CondoMailBox,String> mailTypeColumn = new TableColumn<>("Address a postcard");
        TableColumn<CondoMailBox,String> officerReceivedNameColumn = new TableColumn<>("Officer receiving mail");
        TableColumn<CondoMailBox,String> dateTimeOfficerReceivedColumn = new TableColumn<>("DateTime Officer Received");
        TableColumn<CondoMailBox,String> receiveStatusColumn = new TableColumn<>("Receive Status");
        TableColumn<CondoMailBox,String> residentReceivingMailColumn = new TableColumn<>("Resident Receiving Mail");
        TableColumn<CondoMailBox,String> receiveMailFromOfficerColumn = new TableColumn<>("Received Mail From Officer");
        TableColumn<CondoMailBox,String> dateTimeResidentReceivedColumn = new TableColumn<>("DateTime Resident Received");

        if (showStatus.equalsIgnoreCase("see only received mail")){
            condoMailBoxList = mailDataSource.getMailBoxData(condoMailBoxList);
            condoMailBoxList.getMailByStatus("received");
        }else if (showStatus.equalsIgnoreCase("see all mail")){
            condoMailBoxList = mailDataSource.getMailBoxData(condoMailBoxList);
        }else if (showStatus.equalsIgnoreCase("standard")){
            condoMailBoxList = mailDataSource.getMailBoxData(condoMailBoxList);
            condoMailBoxList.getMailByStatus("Unreceived");
        }else if (showStatus.equalsIgnoreCase("expect roomID")){
            condoMailBoxList = mailDataSource.getMailBoxData(condoMailBoxList);
            CondoRoom condoRoom = new SmallCondoRoom(buildingChoiceBox.getValue(),floorChoiceBox.getValue(),roomChoiceBox.getValue().substring(5,7),"","");
            condoMailBoxList.getMailByRoomID(condoRoom);
        }
        condoMailBoxObservableList = FXCollections.observableArrayList(condoMailBoxList.toList());

        mailTypeColumn.setMinWidth(0);
        mailTypeColumn.setCellValueFactory(new PropertyValueFactory<>("mail"));
        officerReceivedNameColumn.setMinWidth(0);
        officerReceivedNameColumn.setStyle("-fx-alignment: CENTER;");
        officerReceivedNameColumn.setCellValueFactory(new PropertyValueFactory<>("officerReceivingMail"));
        dateTimeOfficerReceivedColumn.setMinWidth(0);
        dateTimeOfficerReceivedColumn.setStyle("-fx-alignment: CENTER;");
        dateTimeOfficerReceivedColumn.setCellValueFactory(new PropertyValueFactory<>("dateTimeOfficerReceived"));
        receiveStatusColumn.setMinWidth(0);
        receiveStatusColumn.setStyle("-fx-alignment: CENTER;");
        receiveStatusColumn.setCellValueFactory(new PropertyValueFactory<>("receiveStatus"));
        residentReceivingMailColumn.setMinWidth(0);
        residentReceivingMailColumn.setStyle("-fx-alignment: CENTER;");
        residentReceivingMailColumn.setCellValueFactory(new PropertyValueFactory<>("residentReceivingMail"));
        receiveMailFromOfficerColumn.setMinWidth(0);
        receiveMailFromOfficerColumn.setStyle("-fx-alignment: CENTER;");
        receiveMailFromOfficerColumn.setCellValueFactory(new PropertyValueFactory<>("receiveMailFromOfficer"));
        dateTimeResidentReceivedColumn.setMinWidth(0);
        dateTimeResidentReceivedColumn.setStyle("-fx-alignment: CENTER;");
        dateTimeResidentReceivedColumn.setCellValueFactory(new PropertyValueFactory<>("dateTimeResidentReceived"));

        mailBoxTableView.getColumns().clear();
        mailBoxTableView.getColumns().addAll(mailTypeColumn,officerReceivedNameColumn,dateTimeOfficerReceivedColumn,receiveStatusColumn,residentReceivingMailColumn,receiveMailFromOfficerColumn,dateTimeResidentReceivedColumn);
        mailBoxTableView.getSortOrder().add(dateTimeOfficerReceivedColumn);
        mailBoxTableView.setItems(condoMailBoxObservableList);

        dateTimeOfficerReceivedColumn.setSortType(TableColumn.SortType.DESCENDING);
        mailBoxTableView.getSortOrder().add(dateTimeOfficerReceivedColumn);
        mailBoxTableView.setItems(condoMailBoxObservableList);
    }

    private void showSelectedMail(CondoMailBox condoMailBox){
        mailImage.setImage(new Image(condoMailBox.getMail().getImageMail()));
        residentReceivedBtn.setDisable(false);
    }
    private String getDateTime(){
        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy / HH:mm:ss");
        String formattedDate = myDateObj.format(myFormatObj);
        return formattedDate;
    }
    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}
