package mailBox.controller.officerController;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mailBox.Main;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.models.mailsModels.CondoMailBox;
import mailBox.models.mailsModels.CondoMailBoxList;
import mailBox.models.mailsModels.Mail;
import mailBox.models.roomModels.CondoRoom;
import mailBox.models.roomModels.CondoRoomList;
import mailBox.models.roomModels.SmallCondoRoom;
import mailBox.services.accountServices.CondoAccountDataSource;
import mailBox.services.mailServices.MailDataSource;
import mailBox.services.mailServices.MailFileDataSource;
import mailBox.services.roomServices.CondoRoomDataSource;
import mailBox.services.roomServices.CondoRoomFileDataSource;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MailCreateController {
    private CurrentUser currentUser;
    private CondoAccount condoAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountList accounts;
    private CondoRoom condoRoom;
    private CondoRoomDataSource condoRoomDataSource;
    private CondoRoomList condoRoomList;
    private Mail mail;
    private CondoMailBox condoMailBox;
    private CondoMailBoxList condoMailBoxList;
    private MailDataSource mailDataSource;
    private String imageLocation;

    @FXML private Button createBtn;
    @FXML private Button backBtn;
    @FXML private Button uploadPictureBtn;
    @FXML private Button goToMailBoxBtn;
    @FXML private TextField receiverNameTextField;
    @FXML private TextField receiverLastNameTextField;
    @FXML private TextField senderNameTextField;
    @FXML private TextField mailWidthTextField;
    @FXML private TextField mailLengthTextField;
    @FXML private ChoiceBox<String> buildingChoiceBox;
    @FXML private ChoiceBox<String> floorChoiceBox;
    @FXML private ChoiceBox<String> roomChoiceBox;
    @FXML private ImageView mailImage;
    @FXML private Label currentAccLabel;

    @FXML public void initialize() throws IOException {
        condoRoomDataSource = new CondoRoomFileDataSource("data","CondoRoom.csv");
        mailDataSource = new MailFileDataSource("data","MailBox.csv");
        buildingChoiceBox.getItems().addAll("A","B");
        buildingChoiceBox.setValue("");
        floorChoiceBox.getItems().addAll("01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20");
        floorChoiceBox.setValue("");
        roomChoiceBox.getItems().addAll("Room 01 : 1 bed","Room 02 : 1 bed","Room 03 : 2 beds","Room 04 : 1 bed","Room 05 : 1 bed","Room 06 : 2 beds","Room 07 : 4 beds","Room 08 : 1 bed","Room 09 : 2 beds","Room 10 : 1 bed");
        roomChoiceBox.setValue("");
        Platform.runLater(()->{
            currentAccLabel.setText("OFFICER : "+currentUser.getCurrentAccount().getName());
        });
    }
    @FXML public void handleUploadPictureBtnOnAction(ActionEvent event)throws IOException {
        File jarDir = null;
        File codeDir = null;
        try {
            jarDir = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
            codeDir = jarDir.getParentFile()/*.getParentFile()*/;
//            System.out.println("jar directory: "+codeDir);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        JFileChooser fileChooser = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("JPEG file", "jpg", "jpeg");
        fileChooser.setCurrentDirectory(new File(String.valueOf(codeDir.getParent()) +"/classes" + "/images" + "/mail"));
        fileChooser.setDialogTitle("Choose your mail image");
        fileChooser.setFileFilter(filter);
        fileChooser.showOpenDialog(null);
        if (fileChooser.getSelectedFile() != null){
            imageLocation = "/images/mail/" + String.valueOf(fileChooser.getSelectedFile().getName());
            mailImage.setImage(new Image(imageLocation));
        }
    }
    @FXML public void handleCreateBtnOnAction(ActionEvent event)throws IOException {
        if (receiverNameTextField.getText().matches("[a-zA-Z ]+$") && receiverLastNameTextField.getText().matches("[a-zA-Z ]+$") &&senderNameTextField.getText().matches("[a-zA-Z ]+$") && mailLengthTextField.getText().matches("[0-9.]+$") && mailWidthTextField.getText().matches("[0-9.]+$")){
            String name = receiverNameTextField.getText()+" "+receiverLastNameTextField.getText();
            if (buildingChoiceBox.getValue().matches("[A-B]") && floorChoiceBox.getValue().matches("[0-9]+$") && roomChoiceBox.getValue().substring(5,7).matches("[0-9a-zA-Z]+$")){
                if (mailImage.getImage() != null){
                    CondoRoom receiverRoom = new SmallCondoRoom(buildingChoiceBox.getValue(),floorChoiceBox.getValue(),roomChoiceBox.getValue().substring(5,7),"",name);
                    CondoRoomList condoRoomList = condoRoomDataSource.getCondoRoomData();
                    if(condoRoomList.isInhabitedRoom(receiverRoom)){
                        CondoRoomList condoRoomList1 = condoRoomDataSource.getCondoRoomData();
                        receiverRoom = condoRoomList1.getExpectRoom(receiverRoom);
                        if (receiverRoom.isCorrectOwnerName(name,receiverRoom)){
                            receiverRoom.setOwner(name);
                            Mail mail = new Mail("Mail",receiverRoom,senderNameTextField.getText(),Double.parseDouble(mailWidthTextField.getText()),Double.parseDouble(mailLengthTextField.getText()),imageLocation);
                            CondoMailBox condoMailBox = new CondoMailBox(mail,currentUser.getCurrentAccount().getName(),getDateTime(),"Unreceived","","","");
                            mailDataSource.addMailBoxData(condoMailBox);
                            clearFilledField();
                            Platform.runLater(()->{
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.initStyle(StageStyle.UTILITY);
                                alert.setTitle("Mail Create Success!");
                                alert.setHeaderText(null);
                                alert.setContentText("Mail Was Created Successfully.");
                                alert.showAndWait();
                            });
                        } else {
                            Platform.runLater(()->{
                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                alert.initStyle(StageStyle.UTILITY);
                                alert.setTitle("Mail Create Failed!");
                                alert.setHeaderText(null);
                                alert.setContentText("The receiver name does not match to any members of the room .  please ensure every field is filled correctly.");
                                alert.showAndWait();
                            });
                        }
                    } else {
                        Platform.runLater(()->{
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.initStyle(StageStyle.UTILITY);
                            alert.setTitle("Mail Create Failed!");
                            alert.setHeaderText(null);
                            alert.setContentText("No one lives in this room .  please ensure every room choice box is chooses correctly.");
                            alert.showAndWait();
                        });
                    }
                }else {
                    Platform.runLater(()->{
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.initStyle(StageStyle.UTILITY);
                        alert.setTitle("Mail Create Failed!");
                        alert.setHeaderText(null);
                        alert.setContentText("Please upload your mail image file .  upload your image carefully as you cannot edit image after saving it.");
                        alert.showAndWait();
                    });
                }
            }else {
                Platform.runLater(()->{
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initStyle(StageStyle.UTILITY);
                    alert.setTitle("Mail Create Failed!");
                    alert.setHeaderText(null);
                    alert.setContentText("Please ensure every room choice box is chooses correctly.");
                    alert.showAndWait();
                });
            }
        }else {
            Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Mail Create Failed!");
                alert.setHeaderText(null);
                alert.setContentText("One of the following statements contain characters that aren't allowed " +
                        "\n  - Receiver/Sender name must contain only letters. " +
                        "\n  - Mail length,width and height size. " +
                        "\nPlease ensure every field is filled correctly.");
                alert.showAndWait();
            });
        }
    }
    @FXML public void handleBackBtnOnAction(ActionEvent event)throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/createNewMail.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        CreateNewMailController createNewMailController = loader.getController();
        createNewMailController.setCurrentUser(currentUser);

        stage.show();
    }
    @FXML public void handleGoToMailBoxBtnOnAction(ActionEvent event)throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/officerMailBox.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        OfficerMailBoxListController officerMailBoxListController = loader.getController();
        officerMailBoxListController.setCurrentUser(currentUser);

        stage.show();
    }

    private String getDateTime(){
        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy / HH:mm:ss");
        String formattedDate = myDateObj.format(myFormatObj);
        return formattedDate;
    }
    private void clearFilledField(){
        receiverNameTextField.clear();
        receiverLastNameTextField.clear();
        senderNameTextField.clear();
        mailWidthTextField.clear();
        mailLengthTextField.clear();
        mailImage.setImage(null);
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}
