package mailBox.controller.officerController;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.models.roomModels.CondoRoom;
import mailBox.models.roomModels.CondoRoomList;
import mailBox.models.roomModels.MediumCondoRoom;
import mailBox.models.roomModels.SmallCondoRoom;
import mailBox.services.accountServices.CondoAccountDataSource;
import mailBox.services.roomServices.CondoRoomDataSource;
import mailBox.services.roomServices.CondoRoomFileDataSource;

import java.io.IOException;
import java.util.ArrayList;

public class CondoRoomListController {
    private CurrentUser currentUser;
    private CondoAccount condoAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountList accounts;
    private CondoRoom condoRoom;
    private CondoRoomDataSource condoRoomDataSource;
    private CondoRoomList condoRooms;
    private CondoRoomList expectCondoRooms;
    private ObservableList<CondoRoom> condoRoomObservableList;
    private ObservableList<CondoRoom> allCondoRoomObservableList;

    @FXML private TableView<CondoRoom> roomsTable;
    @FXML private Button backBtn;
    @FXML private Button searchByNameBtn;
    @FXML private Button searchByRoomBtn;
    @FXML private Button showRoomBtn;
    @FXML private Label currentAccLabel;
    @FXML private ChoiceBox<String> buildingChoiceBox;
    @FXML private ChoiceBox<String> floorChoiceBox;
    @FXML private ChoiceBox<String> roomChoiceBox;
    @FXML private TextField expectNameTextField;
    @FXML private TextField expectLastNameTextField;

    @FXML
    public void initialize() throws IOException {
        condoRoomDataSource = new CondoRoomFileDataSource("data","CondoRoom.csv");
        condoRooms = condoRoomDataSource.getCondoRoomData();
        showCondoRoom("show all room",condoRoom);

        buildingChoiceBox.getItems().addAll("A","B");
        buildingChoiceBox.setValue("");
        floorChoiceBox.getItems().addAll("01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20");
        floorChoiceBox.setValue("");
        roomChoiceBox.getItems().addAll("Room 01 : 1 bed","Room 02 : 1 bed","Room 03 : 2 beds","Room 04 : 1 bed","Room 05 : 1 bed","Room 06 : 2 beds","Room 07 : 4 beds","Room 08 : 1 bed","Room 09 : 2 beds","Room 10 : 1 bed");
        roomChoiceBox.setValue("");
    }

    @FXML public void handleSearchByNameBtnOnAction(ActionEvent event) throws IOException{
        CondoRoom expectCondoRoom = new SmallCondoRoom("", "","","",expectNameTextField.getText()+" "+expectLastNameTextField.getText());
        showCondoRoom("search by name",expectCondoRoom);
    }

    @FXML public void handleSearchByRoomBtnOnAction(ActionEvent event) throws IOException{
        if (buildingChoiceBox.getValue().equals("") || floorChoiceBox.getValue().equals("") || roomChoiceBox.getValue().equals("")){ }
        else{
            CondoRoom expectCondoRoom = new SmallCondoRoom(buildingChoiceBox.getValue(),floorChoiceBox.getValue(),roomChoiceBox.getValue().substring(5,7),"","");
            showCondoRoom("search by room",expectCondoRoom);
        }
    }
    @FXML public void handleBackBtnOnAction(ActionEvent event) throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/officer.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        OfficerController officerController = loader.getController();
        officerController.setCurrentUser(currentUser);

        stage.show();
    }
    @FXML public void handleShowRoomBtnOnAction(ActionEvent event)throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/condoRoomList.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        CondoRoomListController condoRoomListController = loader.getController();
        condoRoomListController.setCurrentUser(currentUser);

        stage.show();
    }

    private void showCondoRoom(String getType, CondoRoom condoRoom) throws IOException {
        expectCondoRooms = new CondoRoomList();
        TableColumn<CondoRoom,String> buildingColumn = new TableColumn<>("Building");
        TableColumn<CondoRoom,String> floorColumn = new TableColumn<>("Floor");
        TableColumn<CondoRoom,String> roomColumn = new TableColumn<>("Room");
        TableColumn<CondoRoom,String> roomTypeColumn = new TableColumn<>("RoomType");
        TableColumn<CondoRoom,String> memberColumn = new TableColumn<>("Room Status");
        TableColumn<CondoRoom,String> ownerColumn = new TableColumn<>("Owner");

        if (getType.equalsIgnoreCase("Search by name")){
            CondoRoomList condoRoomList1 = condoRoomDataSource.getCondoRoomData();
            condoRoom = condoRoomList1.getExpectRoomByName(condoRoom.getOwner());
            condoRoom = condoRoomList1.getExpectRoom(condoRoom);
            expectCondoRooms.add(condoRoom); }
        else if (getType.equalsIgnoreCase("Search by room")){
            CondoRoomList condoRoomList = condoRoomDataSource.getCondoRoomData();
            if (condoRoomList.isInhabitedRoom(condoRoom)){
                CondoRoomList condoRoomList1 = condoRoomDataSource.getCondoRoomData();
                condoRoom = condoRoomList1.getExpectRoom(condoRoom);
                expectCondoRooms.add(condoRoom); }
            else {
                expectCondoRooms.add(condoRoom); }
        }else if (getType.equalsIgnoreCase("Show all room")){
            expectCondoRooms = getAllRoom();
            roomsTable.getColumns().addAll(buildingColumn,floorColumn,roomColumn,roomTypeColumn,memberColumn,ownerColumn);
        }
        condoRoomObservableList = FXCollections.observableArrayList(expectCondoRooms.toList());

        buildingColumn.setMinWidth(0);
        buildingColumn.setStyle("-fx-alignment: CENTER;");
        buildingColumn.setCellValueFactory(new PropertyValueFactory<>("building"));
        floorColumn.setMinWidth(0);
        floorColumn.setStyle("-fx-alignment: CENTER;");
        floorColumn.setCellValueFactory(new PropertyValueFactory<>("floor"));
        roomColumn.setMinWidth(0);
        roomColumn.setStyle("-fx-alignment: CENTER;");
        roomColumn.setCellValueFactory(new PropertyValueFactory<>("room"));
        roomTypeColumn.setMinWidth(0);
        roomTypeColumn.setStyle("-fx-alignment: CENTER;");
        roomTypeColumn.setCellValueFactory(new PropertyValueFactory<>("roomType"));
        memberColumn.setMinWidth(0);
        memberColumn.setStyle("-fx-alignment: CENTER;");
        memberColumn.setCellValueFactory(new PropertyValueFactory<>("member"));
        ownerColumn.setMinWidth(0);
        ownerColumn.setStyle("-fx-alignment: CENTER;");
        ownerColumn.setCellValueFactory(new PropertyValueFactory<>("owner"));

        roomsTable.setItems(condoRoomObservableList);
    }

    private CondoRoomList getAllRoom(){
        CondoRoom condoRoom;
        condoRoom = new SmallCondoRoom("", "","","","");
        ArrayList<CondoRoom> tempRoom = new ArrayList<>();
        for (CondoRoom room:condoRooms.toList()){ tempRoom.add(room); }
        for (int i = 0;i<2;i++){
            String floor = "";
            String room = "";
            for (int j = 1;j<=20;j++){
                for (int k = 1;k<=10;k++){
                    if (i == 0){
                        if (j < 10){ floor = "0"+Integer.toString(j); }
                        else { floor = Integer.toString(j); }
                        if (k < 10){ room = "0"+Integer.toString(k); }
                        else { room = Integer.toString(k); }
                        if (k%3 == 0){
                            condoRoom = new MediumCondoRoom("A",floor,room,"0","");
                        }else {
                            condoRoom = new SmallCondoRoom("A",floor,room,"0","");
                        }
                    } else{
                        if (j < 10){ floor = "0"+Integer.toString(j); }
                        else { floor = Integer.toString(j); }
                        if (k < 10){ room = "0"+Integer.toString(k); }
                        else { room = Integer.toString(k); }
                        if (k%3 == 0){
                            condoRoom = new MediumCondoRoom("B",floor,room,"0","");
                        }else {
                            condoRoom = new SmallCondoRoom("B",floor,room,"0","");
                        }
                    }
                    CondoRoomList condoRoomList = condoRoomDataSource.getCondoRoomData();
                    if (!(condoRoomList.isInhabitedRoom(condoRoom))){
                        condoRooms.add(condoRoom); } } } }
        condoRooms.setRoomsMemberStatus();
        return condoRooms;
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}

