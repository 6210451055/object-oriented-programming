package mailBox.controller.homePageController;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import mailBox.Main;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class homeController {

    @FXML private Button startBtn;
    @FXML private Button creditBtn;
    @FXML private Button howBtn;

    @FXML
    public void initialize() throws IOException {
    }

    @FXML public void handleStartBtnOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        stage.show();
    }

    @FXML public void handleCreditBtnOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/credit.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        stage.show();
    }
    @FXML public void handleHowBtnOnAction(ActionEvent event){
        File jarDir = null;
        File codeDir = null;
        try {
            jarDir = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
            codeDir = jarDir.getParentFile().getParentFile();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        try {
            File pdfFile = new File(codeDir+"/classes"+"/info"+"/6210451055.pdf");
            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                } else {
                    System.out.println("Desktop not supported");
                }
            } else {
                System.out.println(codeDir);
                System.out.println("File is not exists");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
