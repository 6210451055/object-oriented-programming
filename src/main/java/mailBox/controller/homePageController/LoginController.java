package mailBox.controller.homePageController;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mailBox.controller.adminController.AdminController;
import mailBox.controller.officerController.OfficerController;
import mailBox.controller.residentController.ResidentController;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;

import java.io.IOException;

public class LoginController {

    private CurrentUser currentUser;
    private CondoAccount loginAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountDataSource accountLoginDataSource;
    private CondoAccountList accounts;

    @FXML private Button loginBtn;
    @FXML private Button registerBtn;
    @FXML private Button backBtn;
    @FXML private TextField loginIdTextField;
    @FXML private PasswordField loginPasswordPin;
    @FXML private Label loginLabel;

    @FXML
    public void initialize() throws IOException {
        accountDataSource = new AccountFileDataSource("data", "CondoAccount.csv");
        accounts = accountDataSource.getCondoAccountsData();
    }

    @FXML public void handleLoginBtnOnAction(ActionEvent event)throws  IOException {
        currentUser = new CurrentUser();
        loginAccount = new CondoAccount("","",loginIdTextField.getText(),loginPasswordPin.getText(),"","");
        if(currentUser.checkIdPasswordThenGet(loginAccount)){
            if (currentUser.getCurrentAccLv().equalsIgnoreCase("admin")) {
                upToDateAccountData();

                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin.fxml"));
                stage.setScene(new Scene(loader.load(), 1000, 800));

                AdminController adminController = loader.getController();
                adminController.setCurrentUser(currentUser);

                stage.show();
            }
            else if (currentUser.getCurrentAccLv().equalsIgnoreCase("resident")){
                upToDateAccountData();

                Button b = (Button) event.getSource();
                Stage stage = (Stage) b.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/resident.fxml"));
                stage.setScene(new Scene(loader.load(), 1000, 800));

                ResidentController residentController = loader.getController();
                residentController.setCurrentUser(currentUser);

                stage.show();
            }
            else if (currentUser.getCurrentAccLv().equalsIgnoreCase("officer")){
                if (currentUser.getCurrentAccount().isSuspendedAccount()){
                    currentUser.getCurrentAccount().loginAttempt();
                    accounts = accountDataSource.getCondoAccountsData();
                    accounts = accounts.setCondoAccount(currentUser.getCurrentAccount(),accounts);
                    accountDataSource.setCondoAccountsData(accounts);
                    Platform.runLater(()->{
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.initStyle(StageStyle.UTILITY);
                        alert.setTitle("Login failed");
                        alert.setHeaderText(null);
                        alert.setContentText("Login failed!. Your account has been suspended .");
                        alert.showAndWait();
                    });
                }else{
                    upToDateAccountData();
                    Button b = (Button) event.getSource();
                    Stage stage = (Stage) b.getScene().getWindow();

                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/officer.fxml"));
                    stage.setScene(new Scene(loader.load(), 1000, 800));

                    OfficerController officerController = loader.getController();
                    officerController.setCurrentUser(currentUser);

                    stage.show();
                }
            }
        }
        else {
            loginLabel.setText("Please try again. \uD83D\uDD04");
            Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Login failed");
                alert.setHeaderText(null);
                alert.setContentText("Login failed!. Wrong ID or Password please ensure every field is filled correctly.");
                alert.showAndWait();
            });
        }
    }

    @FXML public void handleRegisterBtnOnAction(ActionEvent event)throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/residentRegister.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        stage.show();
    }

    @FXML public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/home.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        stage.show();
    }

    private void upToDateAccountData(){
        accounts = accountDataSource.getCondoAccountsData();
        for (CondoAccount account: accounts.toList()){
            if (account.getIDPassword().equals(currentUser.getCurrentAccIdPassword())){
                accounts.remove(account);
                break;
            }
        }
        currentUser.upToDateRecentLogin();
        accounts.add(currentUser.getCurrentAccount());
        accountDataSource.setCondoAccountsData(accounts);
    }
}
