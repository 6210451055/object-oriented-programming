package mailBox.controller.adminController;

import mailBox.controller.accountManagementController.ChangePasswordController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.services.accountServices.CondoAccountDataSource;

import java.io.IOException;

public class AdminController {

    private CurrentUser currentUser;
    private CondoAccount loginAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountList accounts;

    @FXML Button officerListBtn;
    @FXML Button registerOfficerBtn;
    @FXML Button changeAdminPasswordBtn;
    @FXML Button logoutBtn;


    @FXML public void handleOfficerListBtnOnAction (ActionEvent event) throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/officerList.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        OfficerListController officerListController = loader.getController();
        officerListController.setCurrentUser(currentUser);

        stage.show();
    }
    @FXML public void handleRegisterOfficerBtnOnAction (ActionEvent event) throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminRegisterForOfficer.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        OfficerRegisterController officerRegisterController = loader.getController();
        officerRegisterController.setCurrentUser(currentUser);

        stage.show();
    }
    @FXML public void handleChangeAdminPasswordBtnOnAction (ActionEvent event)throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/changePassword.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        ChangePasswordController changePasswordController = loader.getController();
        changePasswordController.setCurrentUser(currentUser);

        stage.show();
    }
    @FXML public void handleLogoutBtnOnAction (ActionEvent event) throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        stage.show();
    }
    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}
