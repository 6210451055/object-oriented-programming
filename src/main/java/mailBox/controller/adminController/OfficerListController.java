package mailBox.controller.adminController;

import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.StageStyle;
import mailBox.controller.adminController.AdminController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;

import java.io.IOException;

public class OfficerListController {
    private CurrentUser currentUser;
    private CondoAccount account;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountList accounts;
    private ObservableList<CondoAccount> accountObservableList;

    @FXML private Button backBtn;
    @FXML private Button suspensionBtn;
    @FXML private TableView<CondoAccount> officerListTable;
    @FXML private ImageView officerImage;
    @FXML private Label officerNameLabel;
    @FXML private Label accountIdLabel;
    @FXML private Label recentLoginLabel;
    @FXML private Label statusLoginLabel;

    @FXML public void initialize() throws IOException {
        accountDataSource = new AccountFileDataSource("data","CondoAccount.csv");
        accounts = accountDataSource.getCondoAccountsData();
        suspensionBtn.setDisable(true);
        showOfficerList();
        officerListTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        showSelectedOfficer(newValue);
                    }
                });
                account = newValue;
            }
        });
    }

    @FXML public void handleSuspensionBtnOnAction (ActionEvent event) throws IOException {
        accounts = accountDataSource.getCondoAccountsData();
        if (account.isSuspendedAccount()){
            account.unsuspendedAccount();
            accounts = accounts.setCondoAccount(account,accounts);
            accountDataSource.setCondoAccountsData(accounts);
            showSelectedOfficer(account);
            officerListTable.refresh();
            Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Unsuspended Account");
                alert.setHeaderText(null);
                alert.setContentText("The account has been Unsuspended .");
                alert.showAndWait();
            });
        }else{
            account.suspendedAccount();
            accounts = accounts.setCondoAccount(account,accounts);
            accountDataSource.setCondoAccountsData(accounts);
            showSelectedOfficer(account);
            officerListTable.refresh();
            Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Suspended Account");
                alert.setHeaderText(null);
                alert.setContentText("The account has been suspended .");
                alert.showAndWait();
            });
        }
    }

    @FXML public void handleBackBtnOnAction (ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        AdminController adminController = loader.getController();
        adminController.setCurrentUser(currentUser);

        stage.show();
    }

    private void showOfficerList() throws IOException{
        CondoAccountList tempAccount = new CondoAccountList();
        for (CondoAccount account: accounts.toList()){
            if (!(account.getAccountLevel().equalsIgnoreCase("officer"))){
                tempAccount.add(account);
            }
        }
        for (CondoAccount account: tempAccount.toList()){
            accounts.remove(account);
        }
        accountObservableList = FXCollections.observableArrayList(accounts.toList());

        TableColumn<CondoAccount,String> nameColumn = new TableColumn<>("Name");
        nameColumn.setMinWidth(0);
        nameColumn.setStyle("-fx-alignment: CENTER;");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("RealName"));

        TableColumn<CondoAccount,String> idColumn = new TableColumn<>("ID");
        idColumn.setMinWidth(0);
        idColumn.setStyle("-fx-alignment: CENTER;");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<CondoAccount,String> recentLoginColumn = new TableColumn<>("Last Logged In (Date/Time)");
        recentLoginColumn.setMinWidth(300);
        recentLoginColumn.setStyle("-fx-alignment: CENTER;");
        recentLoginColumn.setCellValueFactory(new PropertyValueFactory<>("recentLogin"));

        officerListTable.setItems(accountObservableList);
        officerListTable.getColumns().addAll(nameColumn,idColumn,recentLoginColumn);

        recentLoginColumn.setSortType(TableColumn.SortType.DESCENDING);
        officerListTable.getSortOrder().add(recentLoginColumn);

    }

    private void showSelectedOfficer(CondoAccount condoAccount){
        suspensionBtn.setDisable(false);
        officerImage.setImage(new Image(condoAccount.getAccountProfilePicture()));
        centerImage();
        accountIdLabel.setText("ID : "+condoAccount.getId());

        if (condoAccount.getRecentLogin().equalsIgnoreCase("00-00-0000 / 00:00:00 (Never Logged In)")){
            recentLoginLabel.setText("Never Logged In");
        }else{
            recentLoginLabel.setText(condoAccount.getRecentLogin().substring(0,21));
        }
        if (condoAccount.isSuspendedAccount()){
            String[] name = condoAccount.getName().split("!");
            officerNameLabel.setText(name[1]);
            suspensionBtn.setText("Unsuspended Account");
            if (Integer.parseInt(account.checkLoginAttempt()) > 1){
                statusLoginLabel.setText(account.checkLoginAttempt() + " Attempts For Login");
            }else {
                statusLoginLabel.setText(account.checkLoginAttempt() + " Attempt For Login");
            }
        }else {
            officerNameLabel.setText(condoAccount.getName());
            suspensionBtn.setText("Suspended Account");
            statusLoginLabel.setText("");
        }
    }

    private void centerImage() {
        Image img = officerImage.getImage();
        if (img != null) {
            double w = 0;
            double h = 0;

            double ratioX = officerImage.getFitWidth() / img.getWidth();
            double ratioY = officerImage.getFitHeight() / img.getHeight();

            double range = 0;
            if(ratioX >= ratioY) {
                range = ratioY;
            } else {
                range = ratioX;
            }

            w = img.getWidth() * range;
            h = img.getHeight() * range;

            officerImage.setX((officerImage.getFitWidth() - w) / 2);
            officerImage.setY((officerImage.getFitHeight() - h) / 2);

        }
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}
