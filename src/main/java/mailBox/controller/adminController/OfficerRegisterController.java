package mailBox.controller.adminController;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import mailBox.Main;
import mailBox.controller.adminController.AdminController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class OfficerRegisterController {
    private CurrentUser currentUser;
    private CondoAccount condoAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountList accounts;
    private String imageLocation;

    @FXML private Button backBtn;
    @FXML private Button confirmBtn;
    @FXML private Button uploadImageBtn;
    @FXML private TextField nameTextField;
    @FXML private TextField lastNameTextField;
    @FXML private TextField idTextField;
    @FXML private PasswordField passwordPin;
    @FXML private PasswordField rePasswordPin;
    @FXML private ImageView officerImage;

    @FXML public void initialize() {
        accountDataSource = new AccountFileDataSource("data", "CondoAccount.csv");
        accounts = accountDataSource.getCondoAccountsData();
    }

    @FXML public void handleUploadImageBtnOnAction(ActionEvent event) throws  IOException{
        File jarDir = null;
        File codeDir = null;
        try {
            jarDir = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
            codeDir = jarDir.getParentFile();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        JFileChooser fileChooser = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("JPEG file", "jpg", "jpeg");
        fileChooser.setCurrentDirectory(new File(String.valueOf(codeDir.getParent()) + "/classes" + "/images" + "/officer"));
        fileChooser.setDialogTitle("Choose Profile Picture");
        fileChooser.setFileFilter(filter);
        fileChooser.showOpenDialog(null);
        if (fileChooser.getSelectedFile() != null){
            imageLocation = "/images/officer/" + String.valueOf(fileChooser.getSelectedFile().getName());
            officerImage.setImage(new Image(imageLocation));
        }
    }

    @FXML public void handleConfirmBtnOnAction (ActionEvent event) throws IOException {
        if ((nameTextField.getText().matches("[a-zA-Z]+$")) && lastNameTextField.getText().matches("[a-zA-Z]+$") && idTextField.getText().matches("[0-9a-zA-Z]+$") && passwordPin.getText().matches("[0-9a-zA-Z]+$") && officerImage != null){
            String name = nameTextField.getText().substring(0, 1).toUpperCase() + nameTextField.getText().substring(1) + " " + lastNameTextField.getText().substring(0, 1).toUpperCase()+lastNameTextField.getText().substring(1);
            if (!(passwordPin.getText().equals(rePasswordPin.getText()))){
                Platform.runLater(()->{
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initStyle(StageStyle.UTILITY);
                    alert.setTitle("Register failed");
                    alert.setHeaderText(null);
                    alert.setContentText("Password and Re-Password do not match!");
                    alert.showAndWait();
                });
            }
            else {
                CondoAccount checkAcc = new CondoAccount("","",idTextField.getText(),passwordPin.getText(),"","");
                accounts = accountDataSource.getCondoAccountsData();
                if (accounts.findByIdPassword(checkAcc)){
                    Platform.runLater(()->{
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.initStyle(StageStyle.UTILITY);
                        alert.setTitle("Register failed");
                        alert.setHeaderText(null);
                        alert.setContentText("ID or Password already used by another user. Please try again.");
                        alert.showAndWait();
                    });
                }
                else{
                    CondoAccount condoAccount = new CondoAccount("Officer",name,idTextField.getText(),passwordPin.getText(),"00-00-0000 / 00:00:00 (Never Logged In)",imageLocation);
                    accountDataSource.addCondoAccountData(condoAccount);
                    Platform.runLater(()->{
                        nameTextField.clear();
                        lastNameTextField.clear();
                        idTextField.clear();
                        passwordPin.clear();
                        rePasswordPin.clear();
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.initStyle(StageStyle.UTILITY);
                        alert.setTitle("Register");
                        alert.setHeaderText(null);
                        alert.setContentText("Register success!");
                        alert.showAndWait();
                    });
                }
            }
        }
        else {
            Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Register failed");
                alert.setHeaderText(null);
                alert.setContentText("Register failed from one of the following statements" +
                        "\n  - Officer name must contain only letters. " +
                        "\n  - Upload picture. " +
                        "\nPlease ensure every field is filled correctly.");
                alert.showAndWait();
            });
        }
    }


    @FXML public void handleBackBtnOnAction (ActionEvent event) throws IOException {

        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        AdminController adminController = loader.getController();
        adminController.setCurrentUser(currentUser);

        stage.show();
    }
    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}
