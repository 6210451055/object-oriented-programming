package mailBox.controller.accountManagementController;

import mailBox.controller.officerController.OfficerController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mailBox.controller.residentController.ResidentController;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.models.roomModels.CondoRoom;
import mailBox.models.roomModels.CondoRoomList;
import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;
import mailBox.services.roomServices.CondoRoomDataSource;
import mailBox.services.roomServices.CondoRoomFileDataSource;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ResidentRegisterController {

    private CurrentUser currentUser;
    private CondoAccount condoAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountList accounts;

    @FXML private Button backBtn;
    @FXML private Button confirmBtn;
    @FXML private TextField nameTextField;
    @FXML private TextField idTextField;
    @FXML private TextField lastNameTextField;
    @FXML private PasswordField passwordPin;
    @FXML private PasswordField rePasswordPin;

    @FXML public void initialize() throws FileNotFoundException {
        accountDataSource = new AccountFileDataSource("data", "CondoAccount.csv");
        accounts = accountDataSource.getCondoAccountsData();
    }
    @FXML public void handleConfirmBtnOnAction (ActionEvent event) throws IOException {
        if ((nameTextField.getText().matches("[a-zA-Z]+$")) && lastNameTextField.getText().matches("[a-zA-Z]+$")&& idTextField.getText().matches("[0-9a-zA-Z]+$") && passwordPin.getText().matches("[0-9a-zA-Z]+$")){
            String name = nameTextField.getText().substring(0, 1).toUpperCase() + nameTextField.getText().substring(1) + " " + lastNameTextField.getText().substring(0, 1).toUpperCase()+lastNameTextField.getText().substring(1);
            if (!(passwordPin.getText().equals(rePasswordPin.getText()))){
                Platform.runLater(()->{
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initStyle(StageStyle.UTILITY);
                    alert.setTitle("Register failed");
                    alert.setHeaderText(null);
                    alert.setContentText("Password and Re-Password do not match!");
                    alert.showAndWait();
                });
            }
            else {
                CondoAccount checkAcc = new CondoAccount("","",idTextField.getText(),passwordPin.getText(),"","");
                accounts = accountDataSource.getCondoAccountsData();
                if (accounts.findByIdPassword(checkAcc)){
                    Platform.runLater(()->{
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.initStyle(StageStyle.UTILITY);
                        alert.setTitle("Register failed");
                        alert.setHeaderText(null);
                        alert.setContentText("ID and Password already used by another user .  Please try again.");
                        alert.showAndWait();
                    });
                }
                else{
                    CondoAccount condoAccount = new CondoAccount("Resident",name,idTextField.getText(),passwordPin.getText(),"00-00-0000 / 00:00:00 (Never Logged In)","No Profile Picture");
                    accountDataSource.addCondoAccountData(condoAccount);
                    Platform.runLater(()->{
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.initStyle(StageStyle.UTILITY);
                        alert.setTitle("Register");
                        alert.setHeaderText(null);
                        alert.setContentText("Register success!");
                        alert.showAndWait();
                    });
                }
            }
        } else {
            Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Register failed");
                alert.setHeaderText(null);
                alert.setContentText("Register failed! . This name contain characters that aren't allowed please ensure every field is filled correctly.");
                alert.showAndWait();
            });
        }
    }
    @FXML public void handleBackBtnOnAction (ActionEvent event) throws IOException {
        if (currentUser == null){
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
            stage.setScene(new Scene(loader.load(), 1000, 800));

            stage.show();
        }
        else if (currentUser.getCurrentAccount().getAccountLevel().equalsIgnoreCase("Officer")){
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/officer.fxml"));
            stage.setScene(new Scene(loader.load(), 1000, 800));

            OfficerController officerController = loader.getController();
            officerController.setCurrentUser(currentUser);

            stage.show();
        }else if (currentUser.getCurrentAccount().getAccountLevel().equalsIgnoreCase("Resident")){
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/resident.fxml"));
            stage.setScene(new Scene(loader.load(), 1000, 800));

            ResidentController residentController = loader.getController();
            residentController.setCurrentUser(currentUser);

            stage.show();
        }
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }

}
