package mailBox.controller.accountManagementController;

import mailBox.controller.adminController.AdminController;
import mailBox.controller.officerController.OfficerController;
import mailBox.controller.residentController.ResidentController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;

import java.io.IOException;

public class ChangePasswordController {
    private CurrentUser currentUser;
    private CondoAccount condoAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountList accounts;

    @FXML Button confirmBtn;
    @FXML Button backBtn;
    @FXML PasswordField oldPasswordPin;
    @FXML PasswordField newPasswordPin;
    @FXML Label accountLevelTextLabel;
    @FXML Label currentAccLabel;

    @FXML public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                accountLevelTextLabel.setText(currentUser.getCurrentAccount().getAccountLevel());
                currentAccLabel.setText(currentUser.getCurrentAccount().getName());
            }
        });
        accountDataSource = new AccountFileDataSource("data", "CondoAccount.csv");
        accounts = accountDataSource.getCondoAccountsData();
    }
    @FXML public void handleConfirmBtnOnAction(ActionEvent event)throws IOException{
        if (oldPasswordPin.getText().equals(currentUser.getCurrentAccount().getPassword())){
            if (newPasswordPin.getText().matches("[0-9a-zA-z]+$")){
                currentUser.setPasswordCurrentAcc(currentUser.getCurrentAccount(),newPasswordPin.getText());
                Platform.runLater(()->{
                    oldPasswordPin.clear();
                    newPasswordPin.clear();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.initStyle(StageStyle.UTILITY);
                    alert.setTitle("Password Change Successful!");
                    alert.setHeaderText(null);
                    alert.setContentText("Your password was successfully changed.");
                    alert.showAndWait();
                });
            }
            else {
                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initStyle(StageStyle.UTILITY);
                    alert.setTitle("Change password failed");
                    alert.setHeaderText(null);
                    alert.setContentText("Your password must be a combination of alphanumeric characters .  Please try again.");
                    alert.showAndWait();
                });
            }
        }
        else{
            Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Change password failed");
                alert.setHeaderText(null);
                alert.setContentText("Change password failed! .  please ensure every field is filled correctly.");
                alert.showAndWait();
            });
        }
    }
    @FXML public void handleBackBtnOnAction(ActionEvent event)throws IOException{
        if (currentUser.getCurrentAccount().getAccountLevel().equalsIgnoreCase("Admin")){
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/admin.fxml"));
            stage.setScene(new Scene(loader.load(),1000,800));

            AdminController adminController = loader.getController();
            adminController.setCurrentUser(currentUser);

            stage.show();
        }
        else if (currentUser.getCurrentAccount().getAccountLevel().equalsIgnoreCase("Resident")){
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/resident.fxml"));
            stage.setScene(new Scene(loader.load(),1000,800));

            ResidentController residentController = loader.getController();
            residentController.setCurrentUser(currentUser);

            stage.show();
        }
        else if (currentUser.getCurrentAccount().getAccountLevel().equalsIgnoreCase("Officer")){
            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/officer.fxml"));
            stage.setScene(new Scene(loader.load(),1000,800));

            OfficerController officerController = loader.getController();
            officerController.setCurrentUser(currentUser);

            stage.show();
        }
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}
