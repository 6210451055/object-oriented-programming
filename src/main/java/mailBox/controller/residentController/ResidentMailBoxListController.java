package mailBox.controller.residentController;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.models.mailsModels.CondoMailBox;
import mailBox.models.mailsModels.CondoMailBoxList;
import mailBox.models.mailsModels.Mail;
import mailBox.models.roomModels.CondoRoom;
import mailBox.models.roomModels.CondoRoomList;
import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;
import mailBox.services.mailServices.MailDataSource;
import mailBox.services.mailServices.MailFileDataSource;
import mailBox.services.roomServices.CondoRoomDataSource;
import mailBox.services.roomServices.CondoRoomFileDataSource;

import java.io.IOException;

public class ResidentMailBoxListController {
    private CurrentUser currentUser;
    private CondoAccount condoAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountDataSource accountLoginDataSource;
    private CondoAccountList accounts;
    private CondoRoom currentRoom;
    private CondoRoomDataSource condoRoomDataSource;
    private CondoRoomList condoRoomList;
    private Mail mail;
    private CondoMailBox condoMailBox;
    private CondoMailBox selectedMailBox;
    private CondoMailBoxList condoMailBoxList;
    private MailDataSource mailDataSource;
    private ObservableList<CondoMailBox> condoMailBoxObservableList;

    @FXML private Button backBtn;
    @FXML private Button seeBtn;
    @FXML private ImageView mailImage;
    @FXML private Label statusTextField;
    @FXML private Label senderTextField;
    @FXML private Label mailTypeTextField;
    @FXML private TableView<CondoMailBox> mailBoxTableView;

    @FXML public void initialize() throws IOException {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    showMailBoxData("see only unreceived mail");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        accountDataSource = new AccountFileDataSource("data", "CondoAccount.csv");
        condoRoomDataSource = new CondoRoomFileDataSource("data","CondoRoom.csv");
        mailDataSource = new MailFileDataSource("data","MailBox.csv");

        seeBtn.setText("See Only Received Mail");

        mailBoxTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                showSelectedMail(newValue);
                selectedMailBox = newValue;
            }
        });
    }
    @FXML public void handleSeeBtnOnAction(ActionEvent event)throws IOException {
        if (seeBtn.getText().equalsIgnoreCase("See Only Received Mail")) {
            showMailBoxData("see only received mail");
            seeBtn.setText("See Only Unreceived Mail");
        } else {
            showMailBoxData("see only unreceived mail");
            seeBtn.setText("See Only Received Mail");
        }
    }
    @FXML public void handleBackBtnOnAction(ActionEvent event)throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/resident.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        ResidentController residentController = loader.getController();
        residentController.setCurrentUser(currentUser);

        stage.show();
    }
    private void showMailBoxData(String showStatus) throws IOException {
        condoMailBoxList = new CondoMailBoxList();
        condoMailBoxObservableList = FXCollections.observableArrayList(condoMailBoxList.toList());

        TableColumn<CondoMailBox,String> mailTypeColumn = new TableColumn<>("Address a postcard");
        TableColumn<CondoMailBox,String> officerReceivedNameColumn = new TableColumn<>("Officer receiving mail");
        TableColumn<CondoMailBox,String> dateTimeOfficerReceivedColumn = new TableColumn<>("DateTime Officer Received");
        TableColumn<CondoMailBox,String> receiveStatusColumn = new TableColumn<>("Receive Status");
        TableColumn<CondoMailBox,String> residentReceivingMailColumn = new TableColumn<>("Resident Receiving Mail");
        TableColumn<CondoMailBox,String> receiveMailFromOfficerColumn = new TableColumn<>("Received Mail From Officer");
        TableColumn<CondoMailBox,String> dateTimeResidentReceivedColumn = new TableColumn<>("DateTime Resident Received");

        if (showStatus.equalsIgnoreCase("see only unreceived mail")){
            condoMailBoxList = mailDataSource.getMailBoxData(condoMailBoxList);
            condoMailBoxList.getResidentMailByStatus(currentUser.getCurrentAccount(),"unreceived");
        }else if (showStatus.equalsIgnoreCase("see only received mail")){
            condoMailBoxList = mailDataSource.getMailBoxData(condoMailBoxList);
            condoMailBoxList.getResidentMailByStatus(currentUser.getCurrentAccount(),"received");
        }
        condoMailBoxObservableList = FXCollections.observableArrayList(condoMailBoxList.toList());

        mailTypeColumn.setMinWidth(0);
        mailTypeColumn.setCellValueFactory(new PropertyValueFactory<>("mail"));
        officerReceivedNameColumn.setMinWidth(0);
        officerReceivedNameColumn.setStyle("-fx-alignment: CENTER;");
        officerReceivedNameColumn.setCellValueFactory(new PropertyValueFactory<>("officerReceivingMail"));
        dateTimeOfficerReceivedColumn.setMinWidth(0);
        dateTimeOfficerReceivedColumn.setStyle("-fx-alignment: CENTER;");
        dateTimeOfficerReceivedColumn.setCellValueFactory(new PropertyValueFactory<>("dateTimeOfficerReceived"));
        receiveStatusColumn.setMinWidth(0);
        receiveStatusColumn.setStyle("-fx-alignment: CENTER;");
        receiveStatusColumn.setCellValueFactory(new PropertyValueFactory<>("receiveStatus"));
        residentReceivingMailColumn.setMinWidth(0);
        residentReceivingMailColumn.setStyle("-fx-alignment: CENTER;");
        residentReceivingMailColumn.setCellValueFactory(new PropertyValueFactory<>("residentReceivingMail"));
        receiveMailFromOfficerColumn.setMinWidth(0);
        receiveMailFromOfficerColumn.setStyle("-fx-alignment: CENTER;");
        receiveMailFromOfficerColumn.setCellValueFactory(new PropertyValueFactory<>("receiveMailFromOfficer"));
        dateTimeResidentReceivedColumn.setMinWidth(0);
        dateTimeResidentReceivedColumn.setStyle("-fx-alignment: CENTER;");
        dateTimeResidentReceivedColumn.setCellValueFactory(new PropertyValueFactory<>("dateTimeResidentReceived"));

        mailBoxTableView.getColumns().clear();
        mailBoxTableView.getColumns().addAll(mailTypeColumn,officerReceivedNameColumn,dateTimeOfficerReceivedColumn,receiveStatusColumn,residentReceivingMailColumn,receiveMailFromOfficerColumn,dateTimeResidentReceivedColumn);
        mailBoxTableView.getSortOrder().add(dateTimeOfficerReceivedColumn);
        mailBoxTableView.setItems(condoMailBoxObservableList);

        dateTimeOfficerReceivedColumn.setSortType(TableColumn.SortType.DESCENDING);
        mailBoxTableView.getSortOrder().add(dateTimeOfficerReceivedColumn);
        mailBoxTableView.setItems(condoMailBoxObservableList);
    }

    private void showSelectedMail(CondoMailBox condoMailBox){
        mailImage.setImage(new Image(condoMailBox.getMail().getImageMail()));
        mailTypeTextField.setText(condoMailBox.getMail().getMailType());
        senderTextField.setText("Sender : "+condoMailBox.getMail().getSenderName());
        statusTextField.setText("Status : "+condoMailBox.getReceiveStatus());
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}
