package mailBox.controller.residentController;

import mailBox.controller.accountManagementController.ChangePasswordController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import mailBox.controller.residentController.ResidentMailBoxListController;
import mailBox.models.accountModels.CondoAccount;
import mailBox.models.accountModels.CondoAccountList;
import mailBox.models.accountModels.CurrentUser;
import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;

import java.io.IOException;

public class ResidentController {
    private CurrentUser currentUser;
    private CondoAccount condoAccount;
    private CondoAccountDataSource accountDataSource;
    private CondoAccountList accounts;

    @FXML private Label accountLabel;
    @FXML private Button changeResidentPasswordBtn;
    @FXML private Button checkMailboxBtn;
    @FXML private Button logoutBtn;

    @FXML
    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                accountLabel.setText("Welcome : " + currentUser.getCurrentAccount().getName());
            }
        });
        accountDataSource = new AccountFileDataSource("data", "CondoAccount.csv");
        accounts = accountDataSource.getCondoAccountsData();
    }

    @FXML public void handleChangeResidentPasswordBtnOnAction(ActionEvent event)throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/changePassword.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        ChangePasswordController changePasswordController = loader.getController();
        changePasswordController.setCurrentUser(currentUser);

        stage.show();
    }
    @FXML public void handleCheckMailBoxBtnOnAction(ActionEvent event)throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/residentMailBoxList.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        ResidentMailBoxListController residentMailBoxListController = loader.getController();
        residentMailBoxListController.setCurrentUser(currentUser);

        stage.show();
    }
    @FXML public void handleLogoutBtnOnAction(ActionEvent event)throws IOException{
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/login.fxml"));
        stage.setScene(new Scene(loader.load(),1000,800));

        stage.show();
    }

    public void setCurrentUser(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }
}
