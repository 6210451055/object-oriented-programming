package mailBox.models.mailsModels;

import java.io.Serializable;

public class CondoMailBox implements Serializable {
    static final long serialVersionUID = -3121712187355367274L;

    private Mail mail;
    private String officerReceivingMail;
    private String dateTimeOfficerReceived;
    private String receiveStatus;
    private String residentReceivingMail;
    private String receiveMailFromOfficer;
    private String dateTimeResidentReceived;

    public CondoMailBox(Mail mail, String officerReceivingMail, String dateTimeOfficerReceived, String receiveStatus, String residentReceivingMail, String receiveMailFromOfficer, String dateTimeResidentReceived) {
        this.mail = mail;
        this.officerReceivingMail = officerReceivingMail;
        this.dateTimeOfficerReceived = dateTimeOfficerReceived;
        this.receiveStatus = receiveStatus;
        this.residentReceivingMail = residentReceivingMail;
        this.receiveMailFromOfficer = receiveMailFromOfficer;
        this.dateTimeResidentReceived = dateTimeResidentReceived;
    }

    public Mail getMail() {
        return mail;
    }

    public String getOfficerReceivingMail() {
        return officerReceivingMail;
    }

    public String getDateTimeOfficerReceived() {
        return dateTimeOfficerReceived;
    }

    public String getReceiveStatus() {
        return receiveStatus;
    }

    public String getResidentReceivingMail() {
        return residentReceivingMail;
    }

    public String getReceiveMailFromOfficer() {
        return receiveMailFromOfficer;
    }

    public String getDateTimeResidentReceived() {
        return dateTimeResidentReceived;
    }

    public String getReceiverMail(){return mail.getReceiver().getOwner();}

    public void setReceiveStatus(String receiveStatus) {
        this.receiveStatus = receiveStatus;
    }

    public void setResidentReceivingMail(String residentReceivingMail) {
        this.residentReceivingMail = residentReceivingMail;
    }

    public void setReceiveMailFromOfficer(String receiveMailFromOfficer) {
        this.receiveMailFromOfficer = receiveMailFromOfficer;
    }

    public void setDateTimeResidentReceived(String dateTimeResidentReceived) {
        this.dateTimeResidentReceived = dateTimeResidentReceived;
    }

    @Override
    public String toString() {
        return "CondoMailBox{" +
                "mail=" + mail +
                ", officerReceivedName='" + officerReceivingMail + '\'' +
                ", dateTimeOfficerReceived='" + dateTimeOfficerReceived + '\'' +
                ", isReceived='" + receiveStatus + '\'' +
                ", residentReceiveName='" + residentReceivingMail + '\'' +
                ", receiveMailFromOfficerName='" + receiveMailFromOfficer + '\'' +
                ", dateTimeResidentReceived='" + dateTimeResidentReceived + '\'' +
                '}';
    }
}