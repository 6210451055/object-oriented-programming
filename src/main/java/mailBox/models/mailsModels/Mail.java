package mailBox.models.mailsModels;

import mailBox.models.roomModels.CondoRoom;

import java.io.Serializable;

public class Mail implements Serializable {
    static final long serialVersionUID = -6242899315079555441L;

    private String mailType;
    private CondoRoom receiver;
    private String senderName;
    private double mailWidthSize;
    private double mailLengthSize;
    private String imageMail;

    public Mail(String mailType, CondoRoom receiver, String senderName, double mailWidthSize, double mailLengthSize, String imageMail) {
        this.mailType = mailType;
        this.receiver = receiver;
        this.senderName = senderName;
        this.mailWidthSize = mailWidthSize;
        this.mailLengthSize = mailLengthSize;
        this.imageMail = imageMail;
    }

    public String getMailType() { return mailType; }

    public CondoRoom getReceiver() {
        return receiver;
    }

    public String getSenderName() { return senderName; }

    public double getMailWidthSize() {
        return mailWidthSize;
    }

    public double getMailLengthSize() {
        return mailLengthSize;
    }

    public String getImageMail() {
        return imageMail;
    }

    public double getSize(){
        return mailLengthSize*mailLengthSize;
    }

    public String getUniqueField() {
        return receiver +
                senderName +
                mailWidthSize +
                mailLengthSize +
                imageMail;
    }

    @Override
    public String toString() {
        return getMailType()+"           ➤  Receiver name : "+getReceiver().getOwner()+" ➤  To RoomID : "+getReceiver().getRoomID()+" ➤  From : "+getSenderName()+" ➤  MailSize : "+String.format("%.2f", getSize())+" cm²";
    }
}
