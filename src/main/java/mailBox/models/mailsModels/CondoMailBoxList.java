package mailBox.models.mailsModels;

import mailBox.models.accountModels.CondoAccount;
import mailBox.models.roomModels.CondoRoom;
import mailBox.models.roomModels.CondoRoomList;

import java.util.ArrayList;

public class CondoMailBoxList {
    private ArrayList<CondoMailBox> condoMailBoxes;

    public CondoMailBoxList(){condoMailBoxes = new ArrayList<>();}

    public void add(CondoMailBox condoMailBox){condoMailBoxes.add(condoMailBox);}

    public void remove(CondoMailBox condoMailBox){ condoMailBoxes.remove(condoMailBox); }

    public void getResidentMailByStatus(CondoAccount currentAccount, String expectMailStatus){
        CondoMailBoxList tempMail = new CondoMailBoxList();
        for (CondoMailBox mailBox: condoMailBoxes){
            if(expectMailStatus.equalsIgnoreCase("unreceived")){
                if (mailBox.getReceiveStatus().equalsIgnoreCase("Received")) {
                    tempMail.add(mailBox); } }
            else if(expectMailStatus.equalsIgnoreCase("received")) {
                if (mailBox.getReceiveStatus().equalsIgnoreCase("unreceived")) {
                    tempMail.add(mailBox); } }
        }
        for (CondoMailBox mailBox : tempMail.toList()){
            condoMailBoxes.remove(mailBox); }
        tempMail = new CondoMailBoxList();
        //after this line = remove other person mail, left only current account mail
        for (CondoMailBox mailBox : condoMailBoxes){
            if (!(mailBox.getReceiverMail().equalsIgnoreCase(currentAccount.getName()))){
                tempMail.add(mailBox); } }
        for (CondoMailBox mailBox : tempMail.toList()){
            condoMailBoxes.remove(mailBox); }
    }

    public void getMailByStatus(String expectMailStatus){
        CondoMailBoxList tempMail = new CondoMailBoxList();
        for (CondoMailBox mailBox: condoMailBoxes){
            if(expectMailStatus.equalsIgnoreCase("unreceived")){
                if (mailBox.getReceiveStatus().equalsIgnoreCase("Received")) {
                    tempMail.add(mailBox); } }
            else if(expectMailStatus.equalsIgnoreCase("received")) {
                if (mailBox.getReceiveStatus().equalsIgnoreCase("unreceived")) {
                    tempMail.add(mailBox); } }
        }
        for (CondoMailBox mailBox : tempMail.toList()){
            condoMailBoxes.remove(mailBox); }
    }

    public void getMailByRoomID(CondoRoom condoRoom){
        CondoMailBoxList tempMail = new CondoMailBoxList();
        for (CondoMailBox mailBox : condoMailBoxes){
            if (!(mailBox.getMail().getReceiver().getRoomID().equals(condoRoom.getRoomID()))){
                tempMail.add(mailBox); }
        }
        for (CondoMailBox mailBox : tempMail.toList()){
            condoMailBoxes.remove(mailBox);
        }
    }

    public ArrayList<CondoMailBox> toList() {
        return condoMailBoxes;
    }

    @Override
    public String toString() { return "CondoMailBoxList{" + "condoMailBoxes=" + condoMailBoxes + '}'; }
}
