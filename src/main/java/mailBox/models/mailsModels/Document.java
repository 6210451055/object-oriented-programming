package mailBox.models.mailsModels;

import mailBox.models.roomModels.CondoRoom;

public class Document extends Mail {
    static final long serialVersionUID = 4717998073536847331L;

    private String priority;

    public Document(String mailType, CondoRoom receiver, String senderName, double mailWidthSize, double mailLengthSize, String imageMail, String priority) {
        super(mailType, receiver, senderName, mailWidthSize, mailLengthSize, imageMail);
        this.priority = priority;
    }

    public String getPriority() {
        return priority;
    }

    @Override
    public double getSize(){
        return getMailWidthSize()*getMailLengthSize();
    }

    @Override
    public String toString() {
        return getMailType()+" ➤  Receiver name : "+getReceiver().getOwner()+" ➤  To RoomID : "+getReceiver().getRoomID()+" ➤  From : "+getSenderName()
                +" ➤  MailSize : "+String.format("%.2f", getSize())+" cm²"+" ➤  Priority : "+getPriority();
    }
}
