package mailBox.models.mailsModels;

import mailBox.models.roomModels.CondoRoom;

public class Parcel extends Mail {
    static final long serialVersionUID = 3189788397103990734L;

    private double mailHeightSize;
    private String shippingCompany;
    private String trackingNumber;

    public Parcel(String mailType, CondoRoom receiver, String senderName, double mailWidthSize, double mailLengthSize, String imageMail, double mailHeightSize, String shippingCompany, String trackingNumber) {
        super(mailType, receiver, senderName, mailWidthSize, mailLengthSize, imageMail);
        this.mailHeightSize = mailHeightSize;
        this.shippingCompany = shippingCompany;
        this.trackingNumber = trackingNumber;
    }

    public double getMailHeightSize() {
        return mailHeightSize;
    }

    public String getShippingCompany() {
        return shippingCompany;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    @Override
    public String getUniqueField() {
        return mailHeightSize + shippingCompany + trackingNumber;
    }

    @Override
    public double getSize(){
        return getMailHeightSize()*getMailWidthSize()*getMailLengthSize();
    }

    @Override
    public String toString() {
        return getMailType()+"        ➤  Receiver name : "+getReceiver().getOwner()+" ➤  To RoomID : "+getReceiver().getRoomID()+ " ➤  From : "+getSenderName()
                +" ➤  MailSize : "+String.format("%.2f", getSize())+" cm³"+" ➤  Shipping Company : "+ getShippingCompany()+" ➤  Tracking number : "+getTrackingNumber();
    }
}
