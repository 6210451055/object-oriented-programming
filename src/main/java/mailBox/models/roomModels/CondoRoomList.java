package mailBox.models.roomModels;

import mailBox.models.mailsModels.CondoMailBox;
import mailBox.models.mailsModels.CondoMailBoxList;

import java.io.IOException;
import java.util.ArrayList;

public class CondoRoomList {
    private ArrayList<CondoRoom> condoRooms;

    public CondoRoomList() {
        condoRooms = new ArrayList<>();
    }

    public void add(CondoRoom condoRoom){ condoRooms.add(condoRoom); }

    public void remove(CondoRoom condoRoom){condoRooms.remove(condoRoom); }

    public boolean isInhabitedRoom(CondoRoom condoRoom){
        for(CondoRoom room : condoRooms){
            if (room.getRoomID().equals(condoRoom.getRoomID())){
                return true; } }
        return false;
    }

    public CondoRoom getExpectRoom(CondoRoom condoRoom){
        for (CondoRoom room: condoRooms){
            if (room.getRoomID().equals(condoRoom.getRoomID())){
                return room; } }
        return condoRoom;
    }

    public CondoRoom getExpectRoomByName(String expectName) throws IOException {
        for (CondoRoom room: condoRooms){
            if (room.isCorrectOwnerName(expectName,room)){
                return room; } }
        CondoRoom room = new SmallCondoRoom("","","","","");
        return room;
    }

    public void setRoomsMemberStatus() {
        for (CondoRoom condoRoom: condoRooms){
            condoRoom.setRoomMemberStatus();
        }
    }

    public ArrayList<CondoRoom> toList(){return condoRooms; }

    @Override
    public String toString() {
        return "CondoRoom = " + condoRooms;
    }
}
