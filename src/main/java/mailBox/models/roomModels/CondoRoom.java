package mailBox.models.roomModels;

import java.io.*;

abstract public class CondoRoom implements Serializable {
    static final long serialVersionUID = -4006876618936719922L;

    protected String building;
    protected String floor;
    protected String room;
    protected String member;

    public CondoRoom(String building, String floor, String room, String member) {
        this.building = building;
        this.floor = floor;
        this.room = room;
        this.member = member;
    }

    public String getBuilding() {
        return building;
    }

    public String getFloor() {
        return floor;
    }

    public String getRoom() {
        return room;
    }

    public abstract String getOwner();
    public abstract String getMember();
    public abstract boolean addMember(String newMemberName);
    public abstract void setRoomMemberStatus();
    public abstract boolean isCorrectOwnerName(String checkName,CondoRoom currentRoom) throws IOException;
    public abstract void setOwner(String owner);
    public abstract String getRoomType();

    public String getRoomID(){ return building+floor+room;}

    public String toCSV() {
        return building + ",F" + floor + ",R" + room +","+ member;
    }
    @Override
    public String toString() {
        return building +  floor+ room + member;
    }
}
