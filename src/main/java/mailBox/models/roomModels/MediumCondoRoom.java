package mailBox.models.roomModels;

import mailBox.models.accountModels.CondoAccount;

import java.io.IOException;

public class MediumCondoRoom extends CondoRoom {
    private String owner;
    private String roomType;

    public MediumCondoRoom(String building, String floor, String room, String member, String owner) {
        super(building, floor, room, member);
        this.owner = owner;
        this.roomType = "Medium Room-2 Beds";
    }

    @Override
    public String getOwner() { return owner; }

    @Override
    public String getRoomType(){ return roomType; }

    @Override
    public String getMember() {
        return member;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
    @Override
    public boolean addMember(String newMemberName) {
        if (Integer.parseInt(member.substring(0,1)) < 2){
            int x = Integer.parseInt(member.substring(0,1))+1;
            member = String.valueOf(x);
            owner = owner+"&"+newMemberName;
            return true;
        } else return false;
    }

    @Override
    public void setRoomMemberStatus() {
        int x = Integer.parseInt(member.substring(0, 1));
        if (x < 2) {
            this.member = String.valueOf(x) + "/2 Not Full";
        } else {
            this.member = String.valueOf(x) + "/2 Full";
        }
    }

    @Override
    public boolean isCorrectOwnerName(String checkName,CondoRoom currentRoom) throws IOException {
        String allOwner = currentRoom.getOwner();
        String[] arrOfOwner = allOwner.split("&");
        for (String s : arrOfOwner){
            if (s.equalsIgnoreCase(checkName)){
                return true; } }
        return false;
    }

    @Override
    public String toCSV() {
        return "Medium Room-2 Beds,"+owner+","+super.toCSV();
    }
}
