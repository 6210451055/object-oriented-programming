package mailBox.models.accountModels;

public class CondoAccount {

    private String accountLevel;
    private String name;
    private String id;
    private String password;
    private String recentLogin;
    private String accountProfilePicture;

    public CondoAccount(String accountLevel, String name, String id, String password, String recentLogin, String accountProfilePicture) {
        this.accountLevel = accountLevel;
        this.name = name;
        this.id = id;
        this.password = password;
        this.recentLogin = recentLogin;
        this.accountProfilePicture = accountProfilePicture;
    }

    public String getAccountLevel() {
        return accountLevel;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getRecentLogin() { return recentLogin; }

    public String getAccountProfilePicture() { return accountProfilePicture; }

    public String getIDPassword(){return  id+password;};

    public void setAccount(String accountLevel,String name,String id,String password,String recentLogin,String accountProfilePicture) {
        this.accountLevel = accountLevel;
        this.name = name;
        this.id = id;
        this.password = password;
        this.recentLogin = recentLogin;
        this.accountProfilePicture = accountProfilePicture;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRecentLogin(String dateTime){this.recentLogin = dateTime;}

    public String getRealName() {
        //this method use for show officer name for tableView
        String[] splitName= (this.name).split("!");
        if (isSuspendedAccount()){
            return splitName[1] + " - (Suspended)";
        }
        return "            "+name+"            ";
    }

    public void suspendedAccount(){
        this.name = "0!"+this.name;
    }

    public void unsuspendedAccount(){
        String[] splitName= (this.name).split("!");
        this.name = splitName[1];
    }

    public boolean isSuspendedAccount(){
        String[] splitName= (this.name).split("!");
        if (splitName.length > 1){
            return true;
        }
        return false;
    }

    public String checkLoginAttempt(){
        String[] splitName= (this.name).split("!");
        return splitName[0];
    }

    public void loginAttempt(){
        String[] splitName= (this.name).split("!");
        int attemptCount = Integer.parseInt(splitName[0]) + 1;
        this.name = attemptCount + "!" + splitName[1];
    }

    @Override
    public String toString() {
        return accountLevel + ","
                + name + ","
                + id + ","
                + password;
    }
}
