package mailBox.models.accountModels;

import mailBox.services.accountServices.AccountFileDataSource;
import mailBox.services.accountServices.CondoAccountDataSource;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class CurrentUser {
    private CondoAccount currentAccount;
    private CondoAccountList condoAccountList;
    private CondoAccountDataSource accountDataSource;

    public CurrentUser(){
        condoAccountList = new CondoAccountList();
    }

    public boolean checkIdPasswordThenGet(CondoAccount condoAccount) throws IOException {
        accountDataSource = new AccountFileDataSource("data","CondoAccount.csv");
        condoAccountList = accountDataSource.getCondoAccountsData();
        if(condoAccountList.findByIdPassword(condoAccount)){
            currentAccount = condoAccount;
            return true;
        }
        currentAccount = null;
        return false;
    }

    public void setPasswordCurrentAcc(CondoAccount condoAccount, String newPassword){
        accountDataSource = new AccountFileDataSource("data","CondoAccount.csv");
        condoAccountList = accountDataSource.getCondoAccountsData();
        ArrayList<CondoAccount> tempAccount = new ArrayList<>();
        for (CondoAccount account: condoAccountList.toList()){
            if (!(account.toString().equals(condoAccount.toString()))){
                tempAccount.add(account); }
        }
        condoAccount.setPassword(newPassword);
        tempAccount.add(condoAccount);
        condoAccountList = new CondoAccountList();
        for (CondoAccount account: tempAccount) {
            condoAccountList.add(account);
        }
        accountDataSource.setCondoAccountsData(condoAccountList);
    }

    public CondoAccount getCurrentAccount() { return currentAccount; }

    public String getCurrentAccLv() {return currentAccount.getAccountLevel();}

    public String getCurrentAccIdPassword() {return  currentAccount.getIDPassword();}

    public void upToDateRecentLogin() {currentAccount.setRecentLogin(dateTime());}

    private String dateTime(){
        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy / HH:mm:ss");
        String formattedDate = myDateObj.format(myFormatObj);
        return formattedDate;
    }


    @Override
    public String toString() {
        return "currentAccount=" + currentAccount ;
    }
}
