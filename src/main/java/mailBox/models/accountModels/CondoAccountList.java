package mailBox.models.accountModels;

import java.util.ArrayList;

public class CondoAccountList {
    private ArrayList<CondoAccount> condoAccounts;

    public CondoAccountList() {
        condoAccounts = new ArrayList<>();
    }

    public void add(CondoAccount condoAccount){
        condoAccounts.add(condoAccount);
    }

    public void remove(CondoAccount condoAccount){
        condoAccounts.remove(condoAccount);
    }

    public CondoAccountList setCondoAccount(CondoAccount condoAccount,CondoAccountList condoAccountList){
        for (CondoAccount account: condoAccountList.toList()){
            if (account.getIDPassword().equals(condoAccount.getIDPassword())){
                condoAccountList.remove(account);
                condoAccountList.add(condoAccount);
                break;
            }
        }
        return condoAccountList;
    }

    public boolean findByIdPassword(CondoAccount currentAccount){
        for (CondoAccount account : condoAccounts) {
            if (account.getId().equals(currentAccount.getId()) || account.getPassword().equals(currentAccount.getPassword())) {
                currentAccount.setAccount(account.getAccountLevel(),account.getName(), account.getId(), account.getPassword(),account.getRecentLogin() ,account.getAccountProfilePicture());
                return true;
            }
        }
        return false;
    }

    public boolean isCorrectName(String checkName){
        for (CondoAccount account : condoAccounts){
            if (account.getName().equalsIgnoreCase(checkName)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<CondoAccount> toList() {
        return condoAccounts;
    }

    @Override
    public String toString() {
        return "CondoAccounts = " + condoAccounts;
    }
}