ิสิ่งที่แก้ไขในการตรวจรอบแรก

1. แก้ในส่วนของการสร้าง account ของเจ้าหน้าที่ จะไม่สามารถสมัครโดยใช้ ID ซ้ำได้อีกต่อไป

2. เพิ่มการ polymorphism ในส่วนของ Condo room

3. มีการระบุ font ที่ใช้ในโปรแกรมใน .pdf และ README.md


อธิบายการวางโครงสร้างไฟล์

ใน Repository จะมีทั้งหมด 3 folder 1.KasetsartCondoMailbox 2.data 3.src

1.ใน folder KasersartCondoMailbox จะมีทั้งหมด 2.folder 1.pdf คือ
 
 1.1) folder ชื่อ 6210451055 ด้านในfolder นี้จะเก็บ jar file เอาไว้ และ .csv ที่ jar file ต้องอ่าน
 
 1.2) folder ชื่อ classes จะเก็บพวก .fxml และ รูปภาพ
 
 1.3) file ชื่อ 6210451055.pdf จะเป็นวิธีการใช้งานของโปรแกรม

2.ใน folder data จะเป็นที่เก็บ .csv แต่เป็น csv ของ src ส่วน jar file จะใช้ดังที่กล่างมาข้างต้นข้อ1.1

3.ใน  /src/main
จะมีทั้งหมด 2 Directory ย่อยดังนี้

1./java 2./resources

1.1.ใน /java จะเป็นส่วนของการเก็บพวก Class ไฟล์ที่เขียนโค้ดในการทำงาน

1.2 ใน /java จะมี 1 Package ชื่อ mailBox โค้ดต่างๆจะถูกเก็บในนี้อีกทีนึง

1.2.1. ใน /mailBox จะมีทั้งหมด 3 Package,1 Class ดังนี้ Directory 1.controller 2.models 3.services Java Class 1.Main

1.2.1.1.ใน /controller จะมีทั้งหมด 5 Package 1.accountManagementController 2.adminController 3.officerControll 4.residentController 5.homePageController

1./accountManagementController เก็บ ClassController ที่มีการจัดการ account ของทั้งหมด 3 role ที่สามารถใช้ด้วยกันได้ ได้แก่ admin,officer และ resident

2./adminController เป็นส่วนที่เก็บ ClassController ที่ถูกใช้ในแต่ละหน้าของ role Admin เมื่อทำการ Login เข้าไปโดย role อื่นๆ จะไม่มีส่วนที่ใช้ Class ในหน้านี้เลย

3./officerController เป็นส่วนที่เก็บ ClassController ถูกใช้ในแต่ละหน้าของ role Officer เมื่อทำการ Login เข้าไปโดย role อื่นๆ จะไม่มีส่วนที่ใช้ Class ในหน้านี้เลย

4./residentController เป็นส่วนที่เก็บ ClassController ถูกใช้ในแต่ละหน้าของ role resident เมื่อทำการ Login เข้าไปโดย role อื่นๆ จะไม่มีส่วนที่ใช้ Class ในหน้านี้เลย

5./homePageControler เป็นที่เก็บ ClassController ในหน้า home ก่อนที่จะมีการทำการ login เข้าไป

1.2.1.2.ใน /models จะมีทั้งหมด 3 Package 1.accountModels 2.mailsModels 3.roomModels

1./accountModels เก็บส่วนของ Java Class ที่เกี่ยวข้องกับ account ทั้งหมด

2./mailsModels เก็บส่วนของ Java Class ที่เกี่ยวข้องกับ mail ทั้งหมด

3./roomModels  เก็บส่วนของ Java Class ที่เกี่ยวข้องกับ Condo room ทั้งหมด

1.2.1.3.ใน /servives จะมีทั้งหมด 3 Package 1.accountServices 2.mailServices 3.roomServices

1./accountServices เก็บสิ่งที่เกี่ยวของกับการจัดการ File .csv และ Interface ของ account

2./mailServices เก็บสิ่งที่เกี่ยวของกับการจัดการ File .csv และ Interface ของ mail

3./roomServices เก็บสิ่งที่เกี่ยวของกับการจัดการ File .csv และ Interface ของ Condo room        

2.1.ใน /resources จะเก็บส่วนที่เก็บพวกไฟล์ .fxml,.pdf และ .jpg รูปภาพที่ต้องใช้ในโปรแกม

2.2.ใน /resources จะมีทั้งหมด 2 directory  คือ images และ info ที่เหลือจะเป็น .fxml หน้า GUI ของโปรแกรมทั้งหมด

2.2.1.ใน /images จะมีทั้งหมด 4 directory ได้แก่

1.credit เอาไว้เก็บรูปภาพผู้จัดทำโปรแกรม

2.icon เป็นรูปภาพ icon ที่ใช้ในโปรแกรม

3.mail เป็นรูปภาพจดหมายต่างๆ ที่ใช้ในโปนแกรม

4.officer รูปภาพของเจ้าหน้าที่ส่วนกลาง

2.2.2.ใน /info จะเก็บพวก .pdf ที่ต้องมีในส่วนของโปรแกรม

ใน  /data 

จะมีทั้งหมด 3 ไฟล์ เป็นไฟล์ .csv ทั้งหมดได้แก่

1.CondoAccount.csv สำหรับการบันทึก account 

2.CondoRoom.csv สำหรับการบันทึก ห้อง

3.MailBox.csv สำหรับการบันทึก จดหมาย



วิธีการเข้าใช้งานข้อมูลผู้ใช้ระบบ

โดยตัวโปรแกรมจะใช้ font ทั้งหมด 3 font ดังนี้

-	Bernard MT Condensed

-	Ebrima

-	System

Username และ Password ของแต่ละ Role

Admin(ผู้ดูและระบบ) 

Username : admin	Password : admin

Officer(เจ้าหน้าที่ส่วนกลาง)

Officer Account 1 = Username : officer1	Password : o1111 

Officer Account 2 = Username : officer2	Password : o2222

Officer Account 3 = Username : officer3	Password : o3333

Resident(ผู้เข้าพักอาศัย)

Resident Account 1 = Username : resident1	Password : r1111 

Resident Account 2 = Username : resident2	Password : r2222

Resident Account 3 = Username : resident3	Password : r3333 



อธิบายการ Commit แต่ละครั้ง

1.commit "add maven to project" - 2020‑09‑17	
- ทำการสร้าง maven ในโปรเจ็ค 

2.commit "add maven to project" - 2020‑09‑17
- เปลี่ยนชื่อ groupId จาก sample เป็น MailBox

3.commit "add more page!" - 2020‑09‑20	
- เพิ่มหน้าวางโครง GUI ของ project 

4.commit "create model" - 2020‑09‑24	
- เริ่มสร้างและวางโครงของ model ในส่วนของ mail Class mail,Document,Parcel

5.commit "read and write file" - 2020‑09‑30	
- เรื่มการสร้าง class สำหรับการอ่านและเขียน File ในส่วนของการอ่านและเขียน model Account

6.commit "add account management system" - 2020‑10‑04	
- เพิ่มส่วนของการจัดการ Condo Account การสร้าง Class ของ CondoAccount ส่วนของหน้า GUI ที่สัมพันธ์กับการใช้งาน Account

7.commit "add condo room management system" - 2020‑10‑06
- เพิ่มส่วนของการจัดการ Condo Room การสร้าง Class ที่เกี่ยวข้องของ CondoRoom เพิ่มหน้า GUI ที่สัมพันธ์กับการใช้งาน CondoRoom

8.commit "add Officer login log" - 2020‑10‑08 
- เพิ่มส่วนของการจัดการระบบการเข้าใช้งานวันเวลาของการเข้าใช้งาน Account Officer 

9.commit "add Condo Room feature" - 2020‑10‑09
- เพิ่ม feature ในส่วนของการจัดการห้อง การเพิ่มคนเข้าห้อง การเปลี่ยนสถานะห้องเต็ม/ไม่เต็ม การหาห้อง

10.commit "add mailbox page & create mail page" - 2020-10-10
- การเพิ่มหน้าของส่วน mailbox และ การสร้าง mail

11.commit "add Condo mailbox management and fix/organized the code of CondoRoom management" - 2020-10-14
- เพิ่มส่วนของการจัดการ CondoMailbox,Class ที่เกี่ยวข้องกับ CondoMailBox และเพิ่มรูปภาพของจดหมาย และ ทำการปรับปรุงแก้ไขและจัดระเบียบ code Class CondoRoom,CondoRoomList,การอ่านเขียนไฟล์ CondoRoom

12.commit "add resident check mailbox" - 2020-10-15
- เพิ่ม feature ของการเช็คจดหมายของผู้เข้าพักอาศัย หน้า GUI ของการเช็คจดหมาย

13.commit "hotfix add resident to CondoRoom" - 2020-10-15
- แก้ไขส่วนของ Code การเพิ่มผู้เข้าพักอาศัย

14.commit "hotfix organized condoroom code move the method where it should be" - 2020-10-16
- เเก้ไขและจัดระเบียบโค้ดส่วน ของ Condo Room ย้ายแก้ไข method ให้อยู่ในที่ที่ควรอยู่

15.commit "add suspended account and profile picture officer feature and organized some part of code" - 2020-10-17
- เพิ่ม feature ของการยุติการใช้งาน account ของส่วนกลาง และการเพิ่มรูปของของ account ส่วนกลาง และจัดระเบียบของโค้ด และนำส่วนของการ ไฟล์ LoginLog.csv ออกเนื่องจากไม่มีการใช้งานแล้ว

16.commit "fix bug,error add credit page and pdf file" - 2020-10-17
- แก้ไขบักของตัวโปรแกรม error ต่างๆ เพิ่มหน้า Credit และวิธีใช้ที่หน้า home

17.commit "delete trash file" - 2020-10-17
- ลบไฟล์ขยะที่ไม่ได้ใช้ทิ้ง

18.commit "organized directory add UML,pdf" - 2020‑10‑18	
- จัด Directory ให้อยู่ในที่ที่ถูกต้อง และเพิ่มรูป UML class diagram และ pdf ไฟล์ วิธีการใช้งาน 

19.commit "README.md edited" - 2020‑10‑18
- แก้ไขการเขียนไฟล์ README.md

20.commit "add Add Resident to Room feature and organized code/fix bug" - 2020-10-27
- เปลี่ยนแปลง feature ในการแอดคนเข้าคอนโดโดยการแอดคนเข้าคอนโดจะไม่รวมกับการสมัครแอคเคาต์เหมือนเก่า และ จัดระเบียบ/แก้ไขโค้ดอีกมากมาย

21.commit "fix the project" -2020-11-26
- แก้ไขการสมัคร account ของ officer จะไม่สามารถสมัคร id ซ้ำได้อีกต่อไป และได้เปลี่ยนแปลง font บางส่วน นอกจากนี้มีการเพิ่มการใช้ ploymorphism ในส่วนของ model CondoRoom

21.commit "organized / fix code" - 2020-10-27
- จัดระเบียบและแก้ไขโค้ด

22.commit "add sample .csv data" - 2020-10-27
- เพิ่มตัวอย่างของของไฟล์ .csv

23.commit "fix directory" - 2020-10-29
- จัดเรียง directory ใหม่ให้ดูดีและดูง่ายขึ้น

24.commit "README.md edited" - 2020-10-31
- เขียน README.md เพิ่มเติม

25.commit "fix the proejct" - 2020-11- 26
- account ของ officer ไม่สามารถสร้าง id ซ้ำได้อีกต่อไป และ เพิ่มการ polymorphism ในส่วนของ CondoRoom และแก้ไขไฟล์ pdf

26.commit "edit pdf and README and add more example mail" -2020-11-27
- ทำการแก้ไขไฟล์ .pdf และ README และยังเพิ่มตัวอย่างของเมลล์ 

27.commit "edit README 27-11-2020" - 2020-11-27
- แก้ไข้ไฟล์ README